from ebucore_match.Assignment import Assignment
from ebucore_match.XPathAttribute import XPathAttribute


class ImageFormatMatch:
	src_root_xpath = './ebucore:coreMetadata/ebucore:format'
	assignments = list()
	assignments.append(
		Assignment(
			name='width',
			xpath_src=XPathAttribute(xpath='./ebucore:videoFormat/ebucore:width'),
			xpath_dest=XPathAttribute('./ebucore:width'),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='height',
			xpath_src=XPathAttribute(xpath='./ebucore:videoFormat/ebucore:height'),
			xpath_dest=XPathAttribute('./ebucore:height'),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='colour reference',
			xpath_src=XPathAttribute(
				xpath='./ebucore:videoFormat/ebucore:technicalAttributeString[@typeLabel="ColorSpace"]'),
			xpath_dest=XPathAttribute('./ebucore:technicalAttributeString[@typeLabel="colourReference"]'),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='coded standard description',
			xpath_src=XPathAttribute(xpath='./ebucore:videoFormat[@videoFormatName]', attribute='videoFormatName'),
			xpath_dest=XPathAttribute('./ebucore:technicalAttributeString[@typeLabel="imageCodecStandardReference"]'),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='bit depth',
			xpath_src=XPathAttribute(
				xpath='./ebucore:videoFormat/ebucore:technicalAttributeInteger[@typeLabel="BitDepth"]'),
			xpath_dest=XPathAttribute('./ebucore:technicalAttributeUnsignedInteger[@typeLabel="componentBitdepth"]'),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='aspect ratio numerator',
			xpath_src=XPathAttribute(
				xpath='./ebucore:videoFormat/ebucore:aspectRatio/ebucore:factorNumerator'),
			xpath_dest=XPathAttribute('./ebucore:aspectratio/ebucore:factorNumerator'),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='aspect ratio denominator',
			xpath_src=XPathAttribute(
				xpath='./ebucore:videoFormat/ebucore:aspectRatio/ebucore:factorDenominator'),
			xpath_dest=XPathAttribute('./ebucore:aspectratio/ebucore:factorDenominator'),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='video coding type link',
			xpath_src=XPathAttribute(
				xpath='./ebucore:videoFormat/ebucore:videoEncoding', attribute='typeLink'),
			xpath_dest=XPathAttribute('./ebucore:technicalAttributeUri[@typeLabel="imageCodecStandardReference"]'),
			regex=None)
	)
