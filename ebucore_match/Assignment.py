class Assignment:
	def __init__(self, name, xpath_src, xpath_dest, regex=None, complex_map_fct=None):
		self.name = name
		self.xpath_src = xpath_src
		self.xpath_dest = xpath_dest
		self.regex = regex
		self.complex_map_fct = complex_map_fct
