from RV import RV
from ebucore.Constants import Constants
from ebucore.ReferencedStandard import ReferencedStandards
from ebucore_match.Assignment import Assignment
from ebucore_match.XPathAttribute import XPathAttribute


class ImagePackageContainerFormatMatch:
	@staticmethod
	def container_standard_reference_match_name(in_param, out_param):
		rv = RV.info('Matching container standard name')
		# find container name from ebucore:containerFormat
		container_format_elem = in_param.find('./ebucore:containerFormat', Constants.ns)

		if container_format_elem is None:
			return rv.add_child(RV.warning('Unable to find containerFormat element')), False

		container_format_name = container_format_elem.get("containerFormatName")
		if container_format_name is None:
			rv.add_child(RV.warning('No container format name found'))
			return rv, False
		if container_format_name == 'JPEG 2000':
			container_format_name = 'JP2_FF'

		for referenced_standard in ReferencedStandards.image_package:
			if container_format_name in referenced_standard.short_designation_loc:
				out_param.text = referenced_standard.short_designation_loc
				return RV.info(
					'Successfully matched containerStandardReference: ' +
					referenced_standard.short_designation_loc), True

			elif container_format_name in referenced_standard.short_designation_cpp:
				out_param.text = referenced_standard.short_designation_cpp
				return RV.info(
					'Successfully matched containerStandardReference: ' +
					referenced_standard.short_designation_cpp), True

		return rv.add_child(RV.warning('No match for container standard name')), False

	@staticmethod
	def container_standard_reference_match_uri(in_param, out_param):
		rv = RV.info('Matching container standard uri')

		# find container name from ebucore:containerFormat
		container_format_elem = in_param.find('./ebucore:containerFormat', Constants.ns)

		if container_format_elem is None:
			return rv.add_child(RV.warning('Unable to find containerFormat element')), False

		container_format_name = container_format_elem.get("containerFormatName")
		if container_format_name is None:
			rv.add_child(RV.warning('No container format name found'))
			return rv, False

		if container_format_name == 'JPEG 2000':
			container_format_name = 'JP2_FF'

		for referenced_standard in ReferencedStandards.image_package:
			if container_format_name in referenced_standard.short_designation_loc:
				out_param.text = referenced_standard.link
				return RV.info(
					'Successfully matched containerStandardReference: ' + referenced_standard.link), True

			elif container_format_name in referenced_standard.short_designation_cpp:
				out_param.text = referenced_standard.link
				return RV.info(
					'Successfully matched containerStandardReference: ' + referenced_standard.link), True

		return rv.add_child(RV.warning('No match for container standard uri')), False

	src_root_xpath = './ebucore:coreMetadata/ebucore:format'
	assignments = list()
	assignments.append(
		Assignment(
			name='Container Standard Reference Name',
			xpath_src=XPathAttribute(xpath='.'),
			xpath_dest=XPathAttribute(
				'./ebucore:technicalAttributeString[@typeLabel="containerStandardReference"]'),
			regex=None,
			complex_map_fct=container_standard_reference_match_name.__func__
		)
	)
	assignments.append(
		Assignment(
			name='Container Standard Reference Uri',
			xpath_src=XPathAttribute(xpath='.'),
			xpath_dest=XPathAttribute(
				'./ebucore:technicalAttributeUri[@typeLabel="containerStandardReference"]'),
			regex=None,
			complex_map_fct=container_standard_reference_match_uri.__func__
		)
	)
	assignments.append(
		Assignment(
			name='Container Width',
			xpath_src=XPathAttribute(xpath='./ebucore:videoFormat/ebucore:width'),
			xpath_dest=XPathAttribute(
				'./ebucore:technicalAttributeUnsignedInteger[@typeLabel="containerWidth"]'),
			regex=None
		)
	)
	assignments.append(
		Assignment(
			name='Container Height',
			xpath_src=XPathAttribute(xpath='./ebucore:videoFormat/ebucore:height'),
			xpath_dest=XPathAttribute(
				'./ebucore:technicalAttributeUnsignedInteger[@typeLabel="containerHeight"]'),
			regex=None
		)
	)


