import os
from lxml import etree
from ebucore_match.Assignment import Assignment
from ebucore_match.XPathAttribute import XPathAttribute
from utils.log import log
from ebucore.Constants import Constants
from ebucore_match.ComponentizedPackageFormatMatch import ComponentizedPackageFormatMatch
from RV import RV


class CplFormatMatch:

    @staticmethod
    def get_content_title(cpl_path):
        rv = RV.info('Getting content title from cpl: ' + cpl_path)
        try:
            tree = etree.parse(cpl_path)
            root = tree.getroot()

            for element in root.getiterator():
                if element.tag.endswith('ContentTitleText') or element.tag.endswith('ContentTitle'):
                    return rv, element.text

        except etree.XMLSyntaxError as err:
            return rv.add_child(RV.error('While trying to parse XML, error is: ' + str(cpl_path))), \
                   Constants.unknown_metadata_place_holder

        return rv, Constants.unknown_metadata_place_holder

    @staticmethod
    def cpl_content_title(in_param, out_param):
        rv = RV.info('Matching content title for cpl')
        # find locator to file
        cpl_path_elem = in_param.find('./ebucore:locator', Constants.ns)

        if cpl_path_elem is None:
            return rv.add_child(RV.warning('Unable to find expected "locator" element in ebucore source data')), False

        cpl_path = cpl_path_elem.text
        if os.path.exists(cpl_path) is False:
            return rv.add_child(RV.error('Given CPL file does not exist: ' + str(cpl_path))), False

        rv_sub, referenced_standard = ComponentizedPackageFormatMatch.cpl_type(cpl_path)
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv, None

        if referenced_standard['cpl_type'] not in ['DCP', 'IMF']:
           return rv.add_child(RV.warning('Given path to CPL has unknown CPL type.')), False

        rv_sub, content_title = CplFormatMatch.get_content_title(cpl_path)
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv, False

        out_param.text = content_title
        return RV.info('Successfully matched CPLs content title'), True

    src_root_xpath = './ebucore:coreMetadata/ebucore:format'
    assignments = list()

    assignments.append(
        Assignment(
            name='CPL Type and Subtype',
            xpath_src=XPathAttribute(xpath='.'),
            xpath_dest=XPathAttribute(
                './ebucore:technicalAttributeString[@typeLabel="cplContentTitleText"]'),
            regex=None,
            complex_map_fct=cpl_content_title.__func__
        )
    )




