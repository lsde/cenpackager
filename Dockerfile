FROM python:3

RUN apt-get update && \
    apt-get install -y mediainfo && \
    rm -rf /var/lib/apt/lists/*

RUN pip install --upgrade pip && \
    pip install --no-cache-dir python-magic  && \
    pip install --no-cache-dir lxml && \
    pip install --no-cache-dir pymediainfo

COPY . /opt/cenpackager/src/

ENTRYPOINT ["python", "/opt/cenpackager/src/cenpackager.py"]

CMD ["-h"]

WORKDIR /data

