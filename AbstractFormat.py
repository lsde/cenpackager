from utils.log import log
from lxml import etree
import subprocess
import os
import uuid
import abc
from ebucore.Constants import Constants
from RV import RV
import Config
import re
import lxml


class AbstractFormat:
	__metaclass__ = abc.ABCMeta

	def __init__(self, name, namespace, attributes=[]):
		self.name = name
		self.namespace = namespace
		self.entries = list()
		self.attributes = attributes
		self.elem = None

	@staticmethod
	def arity_comment(xml_doc_parent, xml_element):
		if xml_element.arity == '1':
			xml_doc_parent.append(etree.Comment(Constants.one_convention))
		elif xml_element.arity == '0..1':
			xml_doc_parent.append(etree.Comment(Constants.zero_to_one_convention))
		elif xml_element.arity == '1..n':
			xml_doc_parent.append(etree.Comment(Constants.one_to_n_convention))
		elif xml_element.arity == '0..n':
			xml_doc_parent.append(etree.Comment(Constants.zero_to_n_convention))

	@staticmethod
	def add_children(xml_doc_parent, xml_element, force=False):

		if not Config.include_optional_parameter and force is False:
			if xml_element.arity in ['0..1', '0..n']:
				return

		if Config.include_arity_comments:
			AbstractFormat.arity_comment(xml_doc_parent, xml_element)

		new_elem = etree.SubElement(
			xml_doc_parent, "{%s}" % Constants.ns[xml_element.namespace] + xml_element.name, nsmap=Constants.ns)
		for attribute in xml_element.attributes:
			if attribute.namespace != '':
				new_elem.set(
					"{%s}" % Constants.ns[attribute.namespace] + attribute.name, attribute.default_value)
			else:
				new_elem.set(attribute.name, attribute.default_value)
		if xml_element.child_elements is not None:
			for child in xml_element.child_elements:
				AbstractFormat.add_children(new_elem, child)
		if xml_element.text_value is not None:
			new_elem.text = xml_element.text_value.default_value

	def gen_stub(self):
		rv = RV.info('Generating stub for ' + self.name)
		self.elem = etree.Element(
			"{%s}" % Constants.ns[self.namespace] + self.name, nsmap=Constants.ns)

		for attribute in self.attributes:
			if attribute.namespace != '':
				self.elem.set(
					"{%s}" % Constants.ns[attribute.namespace] + attribute.name, attribute.default_value)
			else:
				self.elem.set(attribute.name, attribute.default_value)

		for xml_attrib in self.entries:
			AbstractFormat.add_children(self.elem, xml_attrib)

		# rv.message = 'Successfully generated stub for ' + self.name
		return rv, self.elem

	@staticmethod
	def xml(media_source_file):

		rv = RV.info("Call mediainfo to generate EBUCore metadata for file: " + media_source_file)

		current_environment = os.environ.copy()
		if os.name == 'posix':
			# copy current environment and add /usr/local/bin: since mediainfo is
			# usually installed at this location on macOS
			current_environment['PATH'] = '/usr/local/bin:' + current_environment['PATH']

		outfile = './' + str(uuid.uuid4()) + '.xml'
		result = subprocess.run(
			'mediainfo "' + media_source_file + '" --Output=EBUCore > "' + outfile + '"',
			stdout=subprocess.PIPE, shell=True, env=current_environment)
		try:
			root = etree.parse(outfile)
		except etree.XMLSyntaxError as err:
			return rv.add_child(RV.error('While trying to parse mediainfo xml file: ' + str(err)))
		finally:
			# cleanup
			if os.path.exists(outfile):
				os.remove(outfile)
		return rv, root

	def match_xpath_metadata(self, media_source_file, metadata_match_instance, xml_metadata_file_in):
		rv = RV.info('Matching xpath metadata for ' + self.name)

		if metadata_match_instance is None:
			rv.add_child(RV.warning('No metadata match instance defined. Aborting metadata collecting process.'))
			return rv, None

		if os.path.isdir(media_source_file):
			rv.add_child(
				RV.error('Given media source file is a directory. Please provide a file instead: ' + media_source_file))
			return rv, None

		rv_sub, media_info_xml = AbstractFormat.xml(media_source_file)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv, None

		media_info_xml = media_info_xml.find(metadata_match_instance.src_root_xpath, Constants.ns)

		if xml_metadata_file_in is None:
			rv_sub, cen_metadata_xml = self.gen_stub()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, None
		else:
			cen_metadata_xml = xml_metadata_file_in

		for assignment in metadata_match_instance.assignments:
			# find in media info
			src_element = media_info_xml.find(assignment.xpath_src.xpath, Constants.ns)
			if src_element is None:
				rv.add_child(RV.warning('Unable to find metadata in mediainfo generated file: ' + assignment.name))
				continue

			# find in media info CPP metadata file
			dst_element = cen_metadata_xml.find(assignment.xpath_dest.xpath, Constants.ns)
			if dst_element is None:
				rv.add_child(RV.warning('Unable to find metadata in CPP metadata file: ' + assignment.name))
				continue

			if assignment.complex_map_fct is not None:
				rv_match = RV.info('Matching ' + assignment.name)
				rv_sub, matched = assignment.complex_map_fct(src_element, dst_element)
				if not rv_sub.is_info():
					rv_match.add_child(rv_sub)
					if rv_sub.is_error():
						rv.add_child(rv_match)
						return rv, None
				else:
					rv.add_child(rv_match)
			else:
				if assignment.xpath_src.attribute == 'text':
					src_value = src_element.text
				else:
					try:
						src_value = src_element.attrib[assignment.xpath_src.attribute]
					except KeyError as _e:
						rv.add_child(
							RV.warning('No parameter "' + assignment.xpath_src.attribute + '" for ' + assignment.name))
						continue

				if assignment.xpath_dest.attribute == 'text':
					dst_element.text = src_value
				else:
					dst_element.attrib[assignment.xpath_dest.attribute] = src_value

				# rv.add_child(RV.info('Successfully matched metadata for ' + assignment.name + ', value: ' + src_value))

		# rv.message = 'Successfully matched xpath metadata for ' + self.name
		return rv, cen_metadata_xml

	@abc.abstractmethod
	def collect_metadata(self, media_source_file, xml_metadata_in):
		return

	def gen_xpaths(self, entries, xpath_rel):
		xpaths = []
		for entry in entries:
			xpath = xpath_rel + '/'
			xpath += entry.namespace + ':' + entry.name

			filter_str = ''
			if entry.attributes is not None:
				filter_str = ''
				num_attributes = len(entry.attributes)
				cnt = 0
				for attr in entry.attributes:
					cnt += 1
					if attr.name == 'typeLabel':
						filter_str += '@' + attr.name + '="' + attr.default_value + '"'
					else:
						filter_str += '@' + attr.name + '="' + attr.default_value + '"'
					if cnt < num_attributes:
						filter_str += ' and '

			if filter_str != '':
				xpath += '[' + filter_str + ']'

			xpaths.append([entry.arity, xpath])
			if entry.child_elements is not None:
				child_xpaths = self.gen_xpaths(entry.child_elements,
											   xpath_rel + '/' + entry.namespace + ':' + entry.name)
				xpaths += child_xpaths
		return xpaths

	@staticmethod
	def replace_brackets_and_add_attributes(source_element, xpath_in):

		try:
			the_element = source_element.xpath('/' + xpath_in, namespaces=Constants.ns)[0]
		except IndexError as _:
			return xpath_in

		# xpath_out = xpath_in
		regex = "\[[0-9]+\]$"
		matches = re.findall(regex, xpath_in)

		if len(matches) == 1:
			if xpath_in.endswith(matches[0]):
				filter_str = ''
				num_attributes = len(the_element.attrib)
				cnt = 0
				for key in the_element.attrib:
					value = the_element.attrib[key]
					cnt += 1
					filter_str += '@' + key + '="' + value + '"'
					if cnt < num_attributes:
						filter_str += ' and '

				if filter_str != '':
					xpath_out = xpath_in.replace(matches[0], '[' + filter_str + ']')

		else:
			label = the_element.get('typeLabel')
			if label is not None:
				xpath_out = xpath_in + '[@typeLabel="' + label + '"]'

		parent_elem = source_element.getparent()
		if parent_elem is not None:
			tree = etree.ElementTree(parent_elem)
			xpath_sub = tree.getpath(parent_elem)

			print(xpath_sub)
			print(xpath_out)

			# split am letzten Slash


			# ersten teil wieder einbringen
			# mit letztem Teil verrechnen

			replaced_xpath = AbstractFormat.replace_brackets_and_add_attributes(parent_elem, xpath_sub)
			xpath_out = xpath_out.replace(xpath_sub, '__REPLACE_POINT__')

			xpath_out = xpath_out.replace('__REPLACE_POINT__', xpath_to_replace)
			# xpath_out = xpath_out.replace(xpath_sub, xpath_to_replace)

		print(xpath_out)
		return xpath_out

	def validate(self, source_element, xpath_abs, line_offset):
		rv = RV.info('Validating ' + self.name + ' with xpath: ' + xpath_abs)

		xpaths = self.gen_xpaths(self.entries, '/' + self.namespace + ':' + self.name)
		# ------------------------------------------------------------------------------
		# Check if arity fits quantity of tags
		# ------------------------------------------------------------------------------
		for arity, xpath in xpaths:
			xpath_rel = '/' + xpath
			xpath_result = source_element.xpath(xpath_rel, namespaces=Constants.ns)
			if len(xpath_result) == 0 and '0' not in arity:
				rv.add_child(RV.error('Missing mandatory child node in ' + self.name + ' for: ' + xpath))
			elif len(xpath_result) > 1 and '..n' not in arity:
				rv.add_child(RV.error('More than one child node in ' + self.name + ' for: ' + xpath))

		# ------------------------------------------------------------------------------
		# Check if all existing tags belong to this element
		# ------------------------------------------------------------------------------
		tree = etree.ElementTree(source_element)

		cnt = 0
		for child in source_element.iter():
			cnt += 1
			if cnt == 1:
				continue

			path_rel = tree.getpath(child)
			path_rel = self.replace_brackets_and_add_attributes(source_element, xpath_abs + path_rel)

			found_xpath = False
			for _, xpath in xpaths:
				if path_rel == xpath:
					found_xpath = True
					break

			if not found_xpath:
				rv.add_child(RV.error('Unknown element found: ' + path_rel))

		return rv

	@staticmethod
	def xsd_schema_check(filepath_abs, path_xsd_schema_rel='./xsd/ebucore.xsd'):
		rv = RV.info('Applying EBUCore XSD schema check for file: ' + filepath_abs)

		with open(path_xsd_schema_rel) as file_xsd:
			doc = etree.parse(file_xsd)
		try:
			schema = etree.XMLSchema(doc)
		except lxml.etree.XMLSchemaParseError as e:
			return rv.add_child(RV.error(str(e)))
		with open(filepath_abs) as file_ebucore:
			doc = etree.parse(file_ebucore)
		try:
			schema.assertValid(doc)
		except lxml.etree.DocumentInvalid as e:
			for error in schema.error_log:
				rv.add_child(RV.error("  Line {}: {}".format(error.line, error.message)))
			return rv
		return rv
