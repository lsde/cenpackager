Prerequisites:
--------------

In order to collect metadata from existing media files, cenpackager calls the mediainfo binary which must be installed on the system. Downloads for various operating systems can be found here: https://mediaarea.net/en/MediaInfo

Please make sure your operating systems path's variable is updated properly so that mediainfo can be found by cenpackager.

cenpackager was developed using python3 - we recommend creating a so called Python virtual environment for running cenpackager but you can also use Python's non virtual environment. Most parts of cenpackager rely on Python's standard libraries. At this point only two additional libraries (lxml and python-magic)need to be added. The commands for adding those libraries from the command line are:

```
pip install lxml
pip install python-magic
```


Basic Usage:
------------

- Create an empty CEN Preservation Package (CPP). Given "MyFirstCenPackage_ISAN0000-0001-8947-0000-8-0000-0000-D" as name for the package use the "-c" option (c=create): 
`python cenpackager.py -c /path/to/CPPs/storage/location/MyFirstCenPackage_ISAN0000-0001-8947-0000-8-0000-0000-D`

- Adding a subpackage to an existing CPP can be done using the "-a" option. In the following example a componentizedPackage (a Digital Cinema Package (DCP) in particular) is added: 
`python cenpackager.py -p /path/to/CPPs/storage/location/MyFirstCenPackage_ISAN0000-0001-8947-0000-8-0000-0000-D -a /path/to/a/DCP/short_multi_cpl_smpte_dcp/PKL_bd012962-830f-45bf-b591-db223d55156e.xml -t componentizedPackage`

NOTE: If adding a componentized package, cenpackager is expecting the packages Packing List (PKL) as parameter.

- Updating a subpackage is useful if missing metadata was added manually. If a subpackage called "componentizedPackage_b4257ed2-079f-4297-9147-9a122e63cb2e" was modified use the following command to recalculate file-pointers and hash values within the CPP: 
`python cenpackager.py -p /path/to/CPPs/storage/location/MyFirstCenPackage -u componentizedPackage_b4257ed2-079f-4297-9147-9a122e63cb2e`


NOTE: Using the command above will trigger a so-called XML schema check in order to make sure that the provided information is valid. As an example the schema checks make sure that XML entries defined for numbers do not carry and text. If the schema check for a whole sub package is not successful, the update process will be stopped. Since it might sometimes be useful to skip this check (for example because you want to store an intermediate working state on a sub package) you can use the -f (f=force) option within the update procedure. Thus, the schema check will not be executed and the subpackage will be updated. Please note that using the -f option might lead to inconsistent states of a CPP.

# Description of Source Code

## Return Value (RV)

Most of cenpackeger’s functions – usually non-trivial functions – return a so-called Return Value-object (RV) as their first value. RV-objects allow returning more detailed information from sub-functions compared to just an object holding the expected return value. The returned information is a combination of a state (Information, Warning or Error) and optional text-values providing additional information about a subfunction’s execution. Thus, more information about whether the function call was successful or not can be returned to the calling function _and_ more detailed information can be returned to the calling function. Usually, the calling function also keeps a RV-object for its own execution-state. To summarize RV-information for functions and sub-functions, RV information can be concatenated.

Let’s have a look on a typical use-case for a RVs. Usually, a function defines its own RV at its very top. 

`rv = RV.info('Writing subpackage metadata for: ' + path_subpackage)`

At the end of the function, this RV object is returned as the first argument. Often, there is more than just the RV object. Within cenpackager, RVs is always returned as first argument.

`return rv, optional_ret_val1, optional_ret_val2`

A function, like shown above, might call other sub-routines to fulfil its task. The following code shows how a RV-sub-call shall be managed:

```
rv_sub, another_return_value = call_to_subroutine(arg1, arg2)
if not rv_sub.is_info():
   rv.add_child(rv_sub)
   if rv_sub.is_error():
      return rv
```

`rv_sub` is the return value of the subroutine’s RV-object, thus holding the status of its execution. There can be additional return-values in case it is necessary to retrieve a result from a subroutine (called `another_return_value` here). Next, it is useful to check, whether the execution of the subroutine was successful or not. This can be determined using the state of `rv_sub`. If RV’s state is „info“, the function returned as expected. If RV’s state is „warning“, some warnings were raised while executing the subroutine. If RV’s state is „error“, some errors were raised while executing the subroutine.

In the given example, the calling function determines, if the RV is non-equal to “info” in a first step. In case the state is not “info”, warnings and errors are added to the parent RV of the calling function. Just in case of an “error”, the calling function decides to return.

The given code-structures can be found frequently in the cenpackeger project since it is one of the basic concepts of the source. Understand the idea and usage of RV-objects is highly recommended before altering the source-code.

## Metadata Matching

One core-functionality of cenpackeger, is to match as much metadata of archived content as possible. Obviously, this is not possible for a metadata such as descriptive metadata and/or very special technical metadata not commonly used in the industry. Nevertheless, cenpackager is design in a way, that automatic matching of metadata can easily be implemented as we will discuss in this chapter. For collecting metadata of added audio-visual content, the well-known command line tool called mediainfo is used. Often information returned from mediainfo can be mapped 1:1 to CPPs.

But before going into detail, let’s have a look how mediainfo is called from cenpacker. Look at class ```AbstractFormat.py``` and see function <code>xml(media_source_file)</code>. Here, <code>mediainfo</code> gets called as subprocess just given the new-added content file. Additionally, <code>mediainfo</code> is configured to output the information in a so-called “EBUCore” fashion using the flag 

`--Output=EBUCore`


EBUCore is used within this standard and thus, most of the metadata can be mapped easily into CEN’s structure. As an example, a call to with a standard video-container-file delivers the following output:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- Generated at 2022-04-21T14:52:00Z by MediaInfoLib - v21.09 -->
<ebucore:ebuCoreMain xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:ebucore="urn:ebu:metadata-schema:ebucore" xmlns:xalan="http://xml.apache.org/xalan" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:ebu:metadata-schema:ebucore https://www.ebu.ch/metadata/schemas/EBUCore/20171009/ebucore.xsd" version="1.8" writingLibraryName="MediaInfoLib" writingLibraryVersion="21.09" dateLastModified="2022-04-21" timeLastModified="14:52:00Z">
	<ebucore:coreMetadata>
		<ebucore:format>
			<ebucore:videoFormat videoFormatName="AVC">
				<ebucore:width unit="pixel">640</ebucore:width>
				<ebucore:height unit="pixel">360</ebucore:height>
				<ebucore:frameRate factorNumerator="1000" factorDenominator="1001">24</ebucore:frameRate>
				<ebucore:aspectRatio typeLabel="display">
					<ebucore:factorNumerator>16</ebucore:factorNumerator>
					<ebucore:factorDenominator>9</ebucore:factorDenominator>
				</ebucore:aspectRatio>
				<ebucore:videoEncoding typeLabel="Baseline@L3" />
				<ebucore:codec>
					<ebucore:codecIdentifier>
						<dc:identifier>avc1</dc:identifier>
					</ebucore:codecIdentifier>
				</ebucore:codec>
				<ebucore:bitRate>451320</ebucore:bitRate>
				<ebucore:scanningFormat>progressive</ebucore:scanningFormat>
				<ebucore:videoTrack trackId="1" trackName="ISO Media file produced by Google Inc. Created on: 06/10/2020." />
				<ebucore:technicalAttributeString typeLabel="ColorSpace">YUV</ebucore:technicalAttributeString>
				<ebucore:technicalAttributeString typeLabel="ChromaSubsampling">4:2:0</ebucore:technicalAttributeString>
				<ebucore:technicalAttributeString typeLabel="colour_primaries">BT.709</ebucore:technicalAttributeString>
				<ebucore:technicalAttributeString typeLabel="transfer_characteristics">BT.709</ebucore:technicalAttributeString>
				<ebucore:technicalAttributeString typeLabel="matrix_coefficients">BT.709</ebucore:technicalAttributeString>
				<ebucore:technicalAttributeString typeLabel="colour_range">Limited</ebucore:technicalAttributeString>
				<ebucore:technicalAttributeString typeLabel="WritingLibrary">x264 core 155 r2901 7d0ff22</ebucore:technicalAttributeString>
				<ebucore:technicalAttributeInteger typeLabel="BitDepth" unit="bit">8</ebucore:technicalAttributeInteger>
				<ebucore:technicalAttributeInteger typeLabel="StreamSize" unit="byte">16388503</ebucore:technicalAttributeInteger>
				<ebucore:technicalAttributeBoolean typeLabel="CABAC">false</ebucore:technicalAttributeBoolean>
				<ebucore:technicalAttributeBoolean typeLabel="MBAFF">false</ebucore:technicalAttributeBoolean>
			</ebucore:videoFormat>
			<ebucore:audioFormat audioFormatName="AAC">
				<ebucore:audioEncoding />
				<ebucore:codec>
					<ebucore:codecIdentifier>
						<dc:identifier>mp4a-40-2</dc:identifier>
					</ebucore:codecIdentifier>
				</ebucore:codec>
				<ebucore:samplingRate>44100</ebucore:samplingRate>
				<ebucore:bitRate>96000</ebucore:bitRate>
				<ebucore:bitRateMode>variable</ebucore:bitRateMode>
				<ebucore:audioTrack trackId="2" trackName="ISO Media file produced by Google Inc. Created on: 06/10/2020." />
				<ebucore:channels>2</ebucore:channels>
				<ebucore:technicalAttributeString typeLabel="ChannelPositions">Front: L R</ebucore:technicalAttributeString>
				<ebucore:technicalAttributeString typeLabel="ChannelLayout">L R</ebucore:technicalAttributeString>
				<ebucore:technicalAttributeInteger typeLabel="StreamSize" unit="byte">3487034</ebucore:technicalAttributeInteger>
			</ebucore:audioFormat>
			<ebucore:containerFormat containerFormatName="MPEG-4">
				<ebucore:containerEncoding formatLabel="MPEG-4" />
				<ebucore:codec>
					<ebucore:codecIdentifier>
						<dc:identifier>mp42</dc:identifier>
					</ebucore:codecIdentifier>
				</ebucore:codec>
				<ebucore:technicalAttributeString typeLabel="FormatProfile">Base Media / Version 2</ebucore:technicalAttributeString>
			</ebucore:containerFormat>
			<ebucore:duration>
				<ebucore:normalPlayTime>PT4M50.575S</ebucore:normalPlayTime>
			</ebucore:duration>
			<ebucore:fileSize>19961127</ebucore:fileSize>
			<ebucore:fileName> A Movie.mp4</ebucore:fileName>
			<ebucore:locator>A Movie.mp4</ebucore:locator>
			<ebucore:technicalAttributeInteger typeLabel="OverallBitRate" unit="bps">549562</ebucore:technicalAttributeInteger>
			<ebucore:dateCreated startDate="2020-06-10" startTime="18:12:47Z" />
			<ebucore:dateModified startDate="2020-06-10" startTime="18:12:47Z" />
		</ebucore:format>
	</ebucore:coreMetadata>
</ebucore:ebuCoreMain>
```

Obviously, there is many information we can use within a CPP archival process. The following sub-chapters will explain how metadata-matching with `mediainfo`’s output is done in trivial, as well as non-trivial, fashion.

But before looking onto the different matching methods, here’s how the code works internally. In sub-folder `ebucore`, each object, representing a metadata-instance, is implemented as an own class deriving important properties from class `AbstractFormat`.

In addition, most of the classes mentioned above have a metadata-matching class stored in sub-folder `ebucore-match`. The calling code of those objects is basically looking for a python-list called `assignments`. Those assignments are used for both matching types, trivial and non-trivial matching. Calling of the assignments happens in function:

`def match_xpath_metadata(self, media_source_file, metadata_match_instance, xml_metadata_file_in)`

in class AbstractFormat.py, which is a parent class for all metadata-related instances in this project. 

All in all, here is the typical call-stack when adding new media to a CPP:

```
1.	PreservationPackage – add_item(…)
2.	PreservationPackage.add_subpackage(…)
3.	rv_sub = content_instance.write_subpackage_metadata(…)
4.	rv_sub, md_elem = _package.collect_metadata(…)
5.	rv_sub, elem = self.match_xpath_metadata(source_file, FormatMatch, xml_in)
```

It is important to note, that function `match_xpath_metadata` will always generate a so-called metadata-stub in case no existing CPP-metadata is provided. Thus, if new files are added to a package and no metadata exists, cenpacker will generate a stub version of desired metadata. The structure of a stub-version itself is defined if each class in sub-folder ebucore (`self.entries` holds information about attributes needed for a CEN package).

In case the reader wants to add metadata matching functionality, the call to 

`AbstractFormat.match_xpath_metadata(…)`

is most important as mentioned above.

### Trivial Matching

**Trivial Matching occurs if we can immediately read the desired value from mediainfo’s output.**

Often, when metadata needs to be matched from mediainfo’s output to internal structures of a CEN-Package, trivial matching is sufficient. Let’s take mediainfo’s output from above. Here, we want to match the source-video’s dimension (width and height) to the appropriate metadata-fields in the standardized CEN-structure. Within cenpackager, metadata of a video is represented by the class `VideoFormat.py`. While adding a video to a CPP, `VideoFormat.py`’s `collect_metadata`-function gets called. Within this call, a subroutine call to `AbstractFormat.match_xpath_metadata` is executed. As 2nd parameter, the object `VideoFormatMatch` is passed, which defines all metadata-mappings for a video. The implementation of the reference software follows the explained structure for almost all media types which can be added to a CEN-package.

Within `VideoFormatMatch`, several assignments were defined. First, the variable `src_root_xpath` should be noted. It defines a common entry-point in the source metadata file (usually generated using mediainfo). In addition, for mapping of _width_ and _height_ from mediainfo’s EBUCore-XML, the following, trivial assignments are necessary:

```xml
assignments.append(
	Assignment(
		name='width',
		xpath_src=XPathAttribute(xpath='./ebucore:videoFormat/ebucore:width'),
		xpath_dest=XPathAttribute('./ebucore:width'),
		regex=None)
)

assignments.append(
	Assignment(
		name='height',
		xpath_src=XPathAttribute(xpath='./ebucore:videoFormat/ebucore:height'),
		xpath_dest=XPathAttribute('./ebucore:height'),
		regex=None)
)
```

Each assignment (defined in class `ebucore_match.Assignment.py`) for trivial matching has a name (_width_ and _height_ in the example above), as well the variables `xpath_src` and `xpath_dest`. `xpath_src` is an xpath-value to locate the desired metadata in the given source file (relatively from `src_root_xpath`) whereas `xpath_dest` holds an xpath defining the storage location for the metadata (usually pointing to an existing variable in the generated metadata-stub).

Both, _width_ and _height_, are located under EBUCore’s videoFormat-tag. Thus, the `source-xpath` can be formulated as shown in the listing above. According to the CPP specification, the destination for both metadata-values are defined as well. 

### Non-trivial Matching

Non-trivial matching is necessary, if metadata cannot be read from mediainfo’s output 1:1 and further processing of the given source files is necessary. As a typical example for such metadata, let’s consider a Digital Cinema Package’s (DCP) conformity. The conformity could be either _Interop_ or _SMPTE_ – due to historical reasons both variants were used frequently in the past. It is common to produce packages following the _SPMTE_ conformance today, but still many _Interop_-DCPs exists. Detecting, whether a package follows _Interop_- or _SMPTE_ conformity is not complex, but as of today, this metadata it is not provided by mediainfo for Trivial Matching. Thus, a special workflow was implemented in the reference software.

Besides providing basic `xpath-values` in mediainfo’s xml files, it is possible to defined so-called callback-functions which will be executed while gathering metadata for a specific media file. Let’s have a look onto the class

`ebucore_match_ComponentizedPackageFormatMatch`

In the assignment-list there is one entry with the name “CPL Type and Subtype”. Besides the already described values for `xpath_src` and `xpath_dest`, this `Assigment` registers a callback function:

`complex_map_fct=cpl_package_type.__func__`

which is also defined in `ComponentizedPackageFormatMatch`.

The underlying architecture of the reference software will call this function while collecting metadata for a recently added DCP. Thus, such functions can be used to gather non-trivial metadata for audiovisual elements. While studying the implementation of the mentioned callback-function several aspects on the provided source files are checked before the conformity is defined and stored in the subpackages metadata.

## Adding sub-packages

Adding a sub-package to an existing CEN Preservation Package (CPP) calls the 

```
def add_subpackage(content_type, path_package, path_item, name_item='')
```

function of class

`
PerservationPackage.py
`

After a couple of checks with regards to the provided content type four main steps are performed for each content type:

1. Creation of a content type such as soundPackage, audiovisualPackage or imagePackage.
2. Copying of all assets to the newly generated sub-package folder.
3. Generation of metadata
4. Update(Generation) of subpackage's Packing List (PKL)
