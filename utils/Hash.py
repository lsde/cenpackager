import os
import hashlib
import base64
from utils.log import log


class Hash:
	@staticmethod
	def get(file_path, algorithm='sha256'):

		if not os.path.exists(file_path):
			log.error("Hash::calculate: Given path does no exist: " + path)
			return None

		h = None
		if algorithm == "sha1":
			h = hashlib.sha1()
		elif algorithm == "sha256":
			h = hashlib.sha256()
		else:
			log.error("Unknown Hash algorithm: " + + algorithm)
			return None

		with open(file_path, 'rb') as file:
			while True:
				# Reading is buffered, so we can read smaller chunks.
				chunk = file.read(h.block_size)
				if not chunk:
					break
				h.update(chunk)

		return h.digest()

	@staticmethod
	def get_string_representation(file_path, algorithm='sha256'):
		digest = Hash.get(file_path, algorithm)
		ret_val = ''
		for b in digest:
			ret_val += '{:02x}'.format(b)
		return ret_val

	@staticmethod
	def get_base_64(file_path, algorithm='sha256'):
		hash = Hash.get(file_path, algorithm)
		b64 = base64.b64encode(hash)
		return b64
