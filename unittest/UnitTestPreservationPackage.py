from AbstractUnitTest import AbstractUnitTest
import datetime
import os
import shutil
import errno
from PreservationPackage import PreservationPackage
from lxml import etree
from utils.utils import num_descriptive_metadata_files
from ebucore.Constants import Constants
import logging
log = logging.getLogger(__name__)


class UnitTestPreservationPackage(AbstractUnitTest):

	package_path = os.path.join('.', 'test_package_' + str(datetime.datetime.now()))
	componentized_package_counter = 0
	extras_package_counter = 0
	image_package_counter = 0
	sound_package_counter = 0
	timed_text_package_counter = 0
	'''
	def check_subpackage_pkl(self, sub_package_pkl_path, num_expected_tech_md_files=1):
		sub_package_path = os.path.dirname(sub_package_pkl_path)
		# check existing
		self.assertEqual(True, os.path.exists(sub_package_pkl_path), msg="Sub-package PKL exists")

		root = etree.parse(sub_package_pkl_path)

		# check if expected number of techMD files exists
		if num_expected_tech_md_files > 0:
			element = root.xpath('/mets:mets/mets:amdSec/mets:techMd/mets:mdRef', namespaces=Constants.ns)
			self.assertEqual(num_expected_tech_md_files, len(element))

			for md_ref_element in element:
				tmp_md_path = md_ref_element.attrib['{' + Constants.ns['xlink'] + '}href']
				sub_package_tech_md_path = os.path.join(sub_package_path, tmp_md_path)
				self.assertEqual(True, os.path.exists(sub_package_tech_md_path))

		# get mets:file and mets:ftpr - make sure they are consistent
		mets_file_list = root.xpath('/mets:mets/mets:fileSec/mets:fileGrp/mets:file', namespaces=Constants.ns)
		mets_file_ftpr_list = root.xpath('/mets:mets/mets:structMap[@TYPE="physical"]/mets:div/mets:div/mets:ftpr', namespaces=Constants.ns)
		self.assertEqual(len(mets_file_list), len(mets_file_ftpr_list))

		for mets_file_ftpr in mets_file_ftpr_list:
			file_ftpr_id = mets_file_ftpr.attrib["ID"]
			id_match = False
			for mets_file in mets_file_list:
				file_id = mets_file.attrib["ID"]
				if file_ftpr_id == file_id:
					id_match = True
					# check file exists
					mets_flocat_element = mets_file.xpath('./mets:FLocat', namespaces=Constants.ns)
					self.assertEqual(1, len(mets_flocat_element))
					data_file_path_rel = mets_flocat_element[0].attrib['{' + Constants.ns['xlink'] + '}href']
					data_file_path_rel = os.path.join(sub_package_path, data_file_path_rel)
					self.assertEqual(True, os.path.exists(data_file_path_rel))
					break
			self.assertEqual(True, id_match)

	def sub_package_paths_by_type(self, sub_package_type):
		sub_package_paths = list()
		preservation_package_path_abs = os.path.join(self.package_path, 'preservationPackingList.xml')
		root = etree.parse(preservation_package_path_abs)

		sub_packages_elements = root.xpath(
			'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div[@TYPE="CPP-Package"]/mets:div[@TYPE="subPackages"]'
			'/mets:mptr', namespaces=Constants.ns)

		for sub_package_element in sub_packages_elements:
			tmp_path = sub_package_element.attrib['{' + Constants.ns['xlink'] + '}href']
			if tmp_path.startswith(sub_package_type):
				sub_package_paths.append(os.path.join(self.package_path, tmp_path))

		return sub_package_paths
	'''
	def test_10_create_new(self):
		# create a new package
		PreservationPackage.create(self.package_path)

		log.info('Generating empty preservation package: ' + self.package_path)

		# check if main folder metadata folder are exsiting
		sub_folders = ["metadata"]
		full_paths = list()
		full_paths.append(self.package_path)
		for sub_folder in sub_folders:
			full_path = os.path.join(os.path.abspath(self.package_path), sub_folder)
			full_paths.append(full_path)

		for path in full_paths:
			self.assertEqual(os.path.exists(path), True)

		# check if Preservation Package PKL and descriptive metadata file exist
		self.assertEqual(os.path.exists(os.path.join(self.package_path, 'preservationPackingList.xml')), True)

		num_desc_md = num_descriptive_metadata_files(os.path.join(self.package_path, 'metadata'))
		self.assertEqual(num_desc_md, 1)

	def test_15_check_ppkl(self):
		preservation_package_path_abs = os.path.join(self.package_path, 'preservationPackingList.xml')
		root = etree.parse(preservation_package_path_abs)

		xpaths = [
			'/mets:mets',
			'/mets:mets/mets:metsHdr',
			'/mets:mets/mets:dmdSec',
			'/mets:mets/mets:dmdSec/mets:mdRef',
			'/mets:mets/mets:amdSec',
			'/mets:mets/mets:fileSec',
			'/mets:mets/mets:fileSec/mets:fileGrp[starts-with(@ID, "ancillaryData_")]',
			'/mets:mets/mets:fileSec/mets:fileGrp[starts-with(@ID, "playlists_")]',
			'/mets:mets/mets:structMap[@TYPE="physical"]',
			'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div[@TYPE="CPP-Package"]/mets:div[@TYPE="ancillaryData"]',
			'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div[@TYPE="CPP-Package"]/mets:div[@TYPE="playlists"]',
			'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div[@TYPE="CPP-Package"]/mets:div[@TYPE="subPackages"]'
		]

		for xpath in xpaths:
			element = root.xpath(xpath, namespaces=Constants.ns)
			log.info('Checking PPKL for element: ' + xpath + ', found: ' + str(len(element)))
			self.assertEqual(1, len(element))

	def test_20_add_three_componentized_packages(self):
		pkl_path_abs = os.path.join(
			'.', 'test_material', 'short_smpte_dcp', 'PKL_9118e2c0-6503-43f7-ab93-621cbb1a7c9c.xml')
		self.assertEqual(os.path.exists(pkl_path_abs), True)
		self.assertEqual(os.path.exists(self.package_path), True)

		for _ in range(0, 3):
			rv_add_item = PreservationPackage.add_item("componentizedPackage", self.package_path, pkl_path_abs, '')
			self.assertEqual(False, rv_add_item.is_error())
			UnitTestPreservationPackage.componentized_package_counter += 1

		# check ppkl entries
		preservation_package_path_abs = os.path.join(self.package_path, 'preservationPackingList.xml')
		root = etree.parse(preservation_package_path_abs)

		sub_packages_elements = root.xpath(
			'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div[@TYPE="CPP-Package"]/mets:div[@TYPE="subPackages"]'
			'/mets:mptr', namespaces=Constants.ns)

		self.assertEqual(3, len(sub_packages_elements))
		for sub_package_element in sub_packages_elements:
			tmp_path = sub_package_element.attrib['{' + Constants.ns['xlink'] + '}href']
			sub_package_pkl_path = os.path.join(self.package_path, tmp_path)
			self.check_subpackage_pkl(sub_package_pkl_path)

		element = root.xpath(
			'/mets:mets/mets:fileSec/mets:fileGrp[starts-with(@ID, "componentized_")]', namespaces=Constants.ns)
		self.assertEqual(3, len(element))

		componentized_count = 0
		for path in os.listdir(self.package_path):
			if path.startswith('componentizedPackage_'):
				componentized_count += 1
				componentized_pkl_path_abs = os.path.join(self.package_path, path, 'packingList.xml')
				self.assertEqual(True, os.path.exists(componentized_pkl_path_abs))

		self.assertEqual(3, componentized_count)

	def test_21_add_one_extras_package(self):

		source_folder = os.path.join('.', 'test_material', 'extras_package')
		self.assertEqual(os.path.exists(source_folder), True)
		self.assertEqual(os.path.exists(self.package_path), True)

		rv_add_item = PreservationPackage.add_item("extraPackage", self.package_path, source_folder, '')
		self.assertEqual(False, rv_add_item.is_error())
		UnitTestPreservationPackage.extras_package_counter += 1

		# get sub-package folder from ppkl
		# check ppkl entries
		preservation_package_path_abs = os.path.join(self.package_path, 'preservationPackingList.xml')
		root = etree.parse(preservation_package_path_abs)

		sub_packages_elements = root.xpath(
			'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div[@TYPE="CPP-Package"]/mets:div[@TYPE="subPackages"]'
			'/mets:mptr', namespaces=Constants.ns)

		for sub_package_element in sub_packages_elements:
			tmp_path = sub_package_element.attrib['{' + Constants.ns['xlink'] + '}href']
			if tmp_path.startswith('extraPackage_'):
				sub_package_pkl_path = os.path.join(self.package_path, tmp_path)
				self.check_subpackage_pkl(sub_package_pkl_path, num_expected_tech_md_files=0)

				sub_package_data_path = os.path.join(self.package_path, os.path.dirname(tmp_path), 'data')
				extras_package_contents = ['example_pdf_file.pdf', 'example_powerpoint_file.pptx', 'example_word_file.docx']

				expected_files = list()
				for extras_package_content in extras_package_contents:
					expected_files.append(os.path.join(sub_package_data_path, extras_package_content))

				for expected_file in expected_files:
					self.assertEqual(True, os.path.exists(expected_file))

	def test_22_add_image_package(self):

		source_folder = os.path.join('.', 'test_material', 'short_j2c_clip')
		self.assertEqual(True, os.path.exists(source_folder))
		self.assertEqual(True, os.path.exists(self.package_path))

		rv_add_item = PreservationPackage.add_item("imagePackage", self.package_path, source_folder, '')
		self.assertEqual(False, rv_add_item.is_error())
		UnitTestPreservationPackage.image_package_counter += 1

		# get sub-package folder from ppkl
		# check ppkl entries
		preservation_package_path_abs = os.path.join(self.package_path, 'preservationPackingList.xml')
		root = etree.parse(preservation_package_path_abs)

		sub_packages_elements = root.xpath(
			'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div[@TYPE="CPP-Package"]/mets:div[@TYPE="subPackages"]'
			'/mets:mptr', namespaces=Constants.ns)

		for sub_package_element in sub_packages_elements:
			tmp_path = sub_package_element.attrib['{' + Constants.ns['xlink'] + '}href']
			if tmp_path.startswith('imagePackage_'):
				sub_package_pkl_path = os.path.join(self.package_path, tmp_path)
				self.check_subpackage_pkl(sub_package_pkl_path)

				sub_package_data_path = os.path.join(self.package_path, os.path.dirname(tmp_path), 'data')

				for i in range(0, 24):
					image_file_name = 'image_' + str(i).zfill(8) + '.j2c'
					image_file_path = os.path.join(sub_package_data_path, image_file_name)
					self.assertEqual(True, os.path.exists(image_file_path))

	def test_23_add_split_bin_image_package(self):
		source_folder = os.path.join('.', 'test_material', 'short_j2c_clip')
		self.assertEqual(os.path.exists(source_folder), True)
		self.assertEqual(os.path.exists(self.package_path), True)

		# copy most of the files
		tmp_source_folder = os.path.join('.', 'test_material', 'short_j2c_clip_splitted')

		try:
			# copy source data to new generated folder
			try:
				shutil.copytree(source_folder, tmp_source_folder)
			except OSError as e:
				# If the error was caused because the source wasn't a directory
				if e.errno == errno.ENOTDIR:
					shutil.copy(source_folder, tmp_source_folder)
				else:
					print('Directory not copied. Error: %s' % e)
					return False

			self.assertEqual(True, os.path.exists(tmp_source_folder))
			self.assertEqual(True, os.path.exists(os.path.join(tmp_source_folder, "image_000009.j2c")))
			os.remove(os.path.join(tmp_source_folder, "image_000009.j2c"))
			self.assertEqual(False, os.path.exists(os.path.join(tmp_source_folder, "image_000009.j2c")))

			rv_add_item = PreservationPackage.add_item("imagePackage", self.package_path, tmp_source_folder, '')
			# Expected warning message:
			# Given image sequence has no continuous file numbering: ./test_material/short_j2c_clip_splitted
			self.assertEqual(True, rv_add_item.is_warning())
			UnitTestPreservationPackage.image_package_counter += 1

			# get sub-package folder from ppkl
			# check ppkl entries
			preservation_package_path_abs = os.path.join(self.package_path, 'preservationPackingList.xml')
			root = etree.parse(preservation_package_path_abs)

			sub_packages_elements = root.xpath(
				'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div[@TYPE="CPP-Package"]/mets:div[@TYPE="subPackages"]'
				'/mets:mptr', namespaces=Constants.ns)

			for sub_package_element in sub_packages_elements:
				tmp_path = sub_package_element.attrib['{' + Constants.ns['xlink'] + '}href']
				if tmp_path.startswith('imagePackage_'):
					sub_package_pkl_path = os.path.join(self.package_path, tmp_path)
					self.check_subpackage_pkl(sub_package_pkl_path)

					sub_package_data_path = os.path.join(self.package_path, os.path.dirname(tmp_path), 'data')

					for i in range(0, 24):
						image_file_name = 'image_' + str(i).zfill(8) + '.j2c'
						image_file_path = os.path.join(sub_package_data_path, image_file_name)
						self.assertEqual(True, os.path.exists(image_file_path))
		except:
			pass
		finally:
			if os.path.exists(tmp_source_folder):
				shutil.rmtree(tmp_source_folder)

	def test_26_add_sound_package(self):

		source_folder = os.path.join('.', 'test_material', 'audio')
		self.assertEqual(os.path.exists(source_folder), True)
		self.assertEqual(os.path.exists(self.package_path), True)

		rv_add_item = PreservationPackage.add_item("soundPackage", self.package_path, source_folder, '')
		# Expected warning message:
		# Given image sequence has no continuous file numbering: ./test_material/short_j2c_clip_splitted
		self.assertEqual(False, rv_add_item.is_error())
		UnitTestPreservationPackage.sound_package_counter += 1

		# get sub-package folder from ppkl
		# check ppkl entries
		preservation_package_path_abs = os.path.join(self.package_path, 'preservationPackingList.xml')
		root = etree.parse(preservation_package_path_abs)

		sub_packages_elements = root.xpath(
			'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div[@TYPE="CPP-Package"]/mets:div[@TYPE="subPackages"]'
			'/mets:mptr', namespaces=Constants.ns)

		for sub_package_element in sub_packages_elements:
			tmp_path = sub_package_element.attrib['{' + Constants.ns['xlink'] + '}href']
			if tmp_path.startswith('soundPackage_'):
				sub_package_pkl_path = os.path.join(self.package_path, tmp_path)
				self.check_subpackage_pkl(sub_package_pkl_path)

	def test_27_add_bin_timed_text_package(self):
		source_folder = os.path.join('.', 'test_material', 'timed_text')
		self.assertEqual(os.path.exists(source_folder), True)
		self.assertEqual(os.path.exists(self.package_path), True)

		rv_add_item = PreservationPackage.add_item("timedTextPackage", self.package_path, source_folder, '')
		self.assertEqual(False, rv_add_item.is_error())
		UnitTestPreservationPackage.timed_text_package_counter += 1

		# get sub-package folder from ppkl
		# check ppkl entries
		preservation_package_path_abs = os.path.join(self.package_path, 'preservationPackingList.xml')
		root = etree.parse(preservation_package_path_abs)

		sub_packages_elements = root.xpath(
			'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div[@TYPE="CPP-Package"]/mets:div[@TYPE="subPackages"]'
			'/mets:mptr', namespaces=Constants.ns)

		for sub_package_element in sub_packages_elements:
			tmp_path = sub_package_element.attrib['{' + Constants.ns['xlink'] + '}href']
			if tmp_path.startswith('timedTextPackage_'):
				sub_package_pkl_path = os.path.join(self.package_path, tmp_path)
				self.check_subpackage_pkl(sub_package_pkl_path, num_expected_tech_md_files=6)

	def test_30_delete_second_componentized_package(self):

		self.assertEqual(True, os.path.exists(self.package_path))

		# get sub-package folder from ppkl
		# check ppkl entries
		preservation_package_path_abs = os.path.join(self.package_path, 'preservationPackingList.xml')
		root = etree.parse(preservation_package_path_abs)

		sub_packages_elements = root.xpath(
			'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div[@TYPE="CPP-Package"]/mets:div[@TYPE="subPackages"]'
			'/mets:mptr', namespaces=Constants.ns)

		componentized_package_cnt = 0
		for sub_package_element in sub_packages_elements:
			tmp_path = sub_package_element.attrib['{' + Constants.ns['xlink'] + '}href']
			if tmp_path.startswith('componentizedPackage_'):
				componentized_package_cnt += 1
				if componentized_package_cnt == 2:
					break

		sub_package_path = os.path.dirname(tmp_path)
		self.assertEqual(True, os.path.exists(os.path.join(self.package_path, sub_package_path)))

		rv_sub = PreservationPackage.delete_item(self.package_path, sub_package_path)
		self.assertEqual(False, rv_sub.is_error())

		root = etree.parse(preservation_package_path_abs)
		sub_packages_elements = root.xpath(
			'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div[@TYPE="CPP-Package"]/mets:div[@TYPE="subPackages"]'
			'/mets:mptr', namespaces=Constants.ns)

		for sub_package_element in sub_packages_elements:
			tmp_path = sub_package_element.attrib['{' + Constants.ns['xlink'] + '}href']
			if tmp_path.startswith('componentizedPackage_'):
				sub_package_pkl_path = os.path.join(self.package_path, tmp_path)
				self.check_subpackage_pkl(sub_package_pkl_path)

	def test_31_delete_sound_package(self):

		sound_sub_packages_paths = self.sub_package_paths_by_type(sub_package_type='soundPackage_')
		self.assertEqual(1, len(sound_sub_packages_paths))

		path_sub_package_rel = os.path.dirname(sound_sub_packages_paths[0])
		path_sub_package_rel = os.path.basename(path_sub_package_rel)
		rv_sub = PreservationPackage.delete_item(self.package_path, path_sub_package_rel)

		self.assertEqual(False, rv_sub.is_error())

		sound_sub_packages_paths = self.sub_package_paths_by_type(sub_package_type='soundPackage_')
		self.assertEqual(0, len(sound_sub_packages_paths))

	def test_40_delete_test_package(self):
		self.assertEqual(os.path.exists(self.package_path), True)
		shutil.rmtree(self.package_path)
		self.assertEqual(os.path.exists(self.package_path), False)



if __name__ == '__main__':
	unittest.main()
