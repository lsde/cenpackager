from utils.log import log


class RV:
	tabs = 0
	INFO = 0
	WARNING = 1
	ERROR = 2

	def __init__(self, severity_level, message):
		self.severity_level = severity_level
		self.children = list()
		self.successors = list()
		self.message = message

	'''
	def append(self, other):
		if other.severity_level > self.severity_level:
			self.severity_level = other.severity_level
		self.successors.append(other)
		return self
	'''

	def add_child(self, other):
		# don't add a bunch of information
		if other.severity_level == RV.INFO:
			return self

		if other.severity_level > self.severity_level:
			self.severity_level = other.severity_level
		self.children.append(other)
		return self

	def dump(self, error_level=0):

		if error_level > self.severity_level:
			return

		output_msg = ''
		for i in range(0, RV.tabs):
			output_msg += '\t'

		output_msg += self.message
		if self.is_error():
			log.error(output_msg)
		elif self.is_warning():
			log.warning(output_msg)
		else:
			log.info(output_msg)

		for child in self.children:
			RV.tabs += 1
			child.dump(error_level)
			RV.tabs -= 1

		for successor in self.successors:
			successor.dump(error_level)

	def is_info(self):
		return self.severity_level == RV.INFO

	def is_warning(self):
		return self.severity_level == RV.WARNING

	def is_error(self):
		return self.severity_level == RV.ERROR

	@staticmethod
	def info(msg):
		return RV(RV.INFO, msg)

	@staticmethod
	def warning(msg):
		return RV(RV.WARNING, msg)

	@staticmethod
	def error(msg):
		return RV(RV.ERROR, msg)


def add_files():
	rv = RV.info('Start Adding Files')
	for i in range(0, 10):
		rv_copy = RV.info('adding: ' + str(i))
		if i == 8:
			rv_copy.message += ' FAILED'
			rv_copy.severity_level = RV.ERROR
		else:
			rv_copy.message += ' OK'
		rv.add_child(rv_copy)

	return rv, 10


def write_pkl():
	rv = RV.info('Start Writing PKL: ')
	rv.message += 'Finished Writing PKL'
	return rv, 20


def main():
	rv = RV.info('Program starting')
	rv_add_files, expected_no = add_files()
	if rv_add_files.is_error():
		rv.add_child(rv_add_files)
		rv.dump()
		return

	else:
		log.info(expected_no)

	rv_write_pkl, expected_no = write_pkl()
	if rv_write_pkl.is_error():
		rv.add_child(rv_write_pkl)
		rv.dump()
		return

	rv.message = 'Program finished with return value: ' + str(rv.severity_level)
	rv.dump()
	return


if __name__ == '__main__':
	main()
