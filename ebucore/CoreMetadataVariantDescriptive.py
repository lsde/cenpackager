from ebucore.XmlTypes import XmlEntry, XmlAttribute, XmlText
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore_match.ImageFormatMatch import ImageFormatMatch as ImageFormatMatch


class CoreMetadataVariantDescriptive(AbstractFormat):
	def __init__(self):
		AbstractFormat.__init__(self, name="coreMetadata", namespace='ebucore')

		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"title",
					"1",
					[
						XmlAttribute(
							'lang',
							Constants.unknown_metadata_place_holder,
							namespace='xml'
						)
					],
					XmlText(Constants.unknown_metadata_place_holder),
					namespace='dc')],
				"title",
				"1",
				[XmlAttribute('typeLabel', Constants.unknown_metadata_place_holder)],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"identifier",
					"1",
					[],
					XmlText(Constants.unknown_metadata_place_holder),
					namespace='dc')],
				"identifier",
				"1..n",
				[
					XmlAttribute('typeLabel', Constants.unknown_metadata_place_holder),
					XmlAttribute('formatLabel', 'URI')
				],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					[
						XmlEntry(
							None,
							"identifier",
							"1",
							[],
							XmlText(Constants.unknown_metadata_place_holder),
							namespace='dc')
					],
					"relationIdentifier",
					"1",
					[],
					None)],
				"isVersionOf",
				"1..n",
				[],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					[
						XmlEntry(
							None,
							"identifier",
							"1",
							[],
							XmlText(Constants.unknown_metadata_place_holder),
							namespace='dc')
					],
					"relationIdentifier",
					"1",
					[],
					None)],
				"hasDataObject",
				"1..n",
				[],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					[
						XmlEntry(
							None,
							"identifier",
							"1",
							[],
							XmlText(Constants.unknown_metadata_place_holder),
							namespace='dc')
					],
					"relationIdentifier",
					"1",
					[],
					None)],
				"isRelatedTo",
				"1..n",
				[],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"released",
					"1",
					[XmlAttribute('year', Constants.unknown_metadata_place_holder)],
					None)],
				"date",
				"1",
				[],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"created",
					"1",
					[
						XmlAttribute('startYear', Constants.unknown_metadata_place_holder),
						XmlAttribute('endYear', Constants.unknown_metadata_place_holder)
					],
					None)],
				"date",
				"1",
				[],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"objectType",
					"1",
					[XmlAttribute('typeLabel', Constants.unknown_metadata_place_holder)],
					XmlText(Constants.unknown_metadata_place_holder))
				],
				"type",
				"0..1",
				[],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					[
						XmlEntry(
							None,
							"organisationName",
							"1",
							[],
							XmlText(Constants.unknown_metadata_place_holder))
					],
					"organisationDetails",
					"1",
					[XmlAttribute('organisationId', Constants.unknown_metadata_place_holder)],
					None)
				,
				XmlEntry(
					None,
					"role",
					"1",
					[XmlAttribute('typeLabel', 'source')],
					None)
				],
				"contributor",
				"0..n",
				[],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"organisationDetails",
					"1",
					[],
					None)
				],
				"metadataProvider",
				"1",
				[],
				None))


	def collect_metadata(self, media_source_file, xml_metadata_file_in):
		cpp_metadata_elem = self.match_xpath_metadata(media_source_file, ImageFormatMatch, xml_metadata_file_in)
		return cpp_metadata_elem
