from ebucore.XmlTypes import XmlEntry, XmlAttribute, XmlText
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore_match.SoundFormatMatch import SoundFormat as SoundFormatMatch
from RV import RV


class AudioFormat(AbstractFormat):
	def __init__(self):
		AbstractFormat.__init__(self, name="audioFormat", namespace='ebucore')

		self.entries.append(XmlEntry(
				None,
				"audioTrackConfiguration",
				"1",
				[
					XmlAttribute('typeNamespace', 'en17650:2022'),
					XmlAttribute('typeLink', 'urn:cen.eu:en17650:2022:ns:metadata#audioTrackConfiguration'),
					XmlAttribute('typeLabel', Constants.unknown_metadata_place_holder)
				],
				None))
		self.entries.append(XmlEntry(
				None,
				"samplingRate",
				"1",
				[],
				XmlText(Constants.unknown_metadata_place_holder)))
		self.entries.append(XmlEntry(
				None,
				"sampleSize",
				"1",
				[],
				XmlText(Constants.unknown_metadata_place_holder)))
		self.entries.append(XmlEntry(
				None,
				"sampleType",
				"1",
				[],
				XmlText(Constants.unknown_metadata_place_holder)))
		self.entries.append(XmlEntry(
				None,
				"audioTrack",
				"1",
				[
					XmlAttribute('trackLanguage', Constants.unknown_optional_metadata_place_holder),
					XmlAttribute('trackId', Constants.unknown_metadata_place_holder),
					XmlAttribute('trackName', Constants.unknown_metadata_place_holder)
				],
				None))
		self.entries.append(XmlEntry(
				None,
				"channels",
				"1",
				[],
				XmlText(Constants.unknown_metadata_place_holder)))
		self.entries.append(XmlEntry(
				None,
				"technicalAttributeString",
				"1",
				[XmlAttribute('typeLabel', 'audioCodecStandardReference')],
				XmlText(Constants.unknown_metadata_place_holder)))
		self.entries.append(XmlEntry(
				None,
				"technicalAttributeString",
				"1",
				[
					XmlAttribute('typeLabel', 'audioContentDescription'),
					XmlAttribute('typeNamespace', 'en17650:2022'),
					XmlAttribute('typeLink', 'urn:cen.eu:en17650:2022:ns:metadata#soundFileTrackKind')
				],
				XmlText(Constants.unknown_metadata_place_holder)))
		self.entries.append(XmlEntry(
				None,
				"technicalAttributeString",
				"1",
				[
					XmlAttribute('typeLabel', 'channelLayout')
				],
				XmlText(Constants.unknown_metadata_place_holder)))
		self.entries.append(XmlEntry(
				None,
				"technicalAttributeString",
				"1",
				[
					XmlAttribute('typeLabel', 'channelLocation')
				],
				XmlText(Constants.unknown_metadata_place_holder)))

		self.entries.append(XmlEntry(
				None,
				"technicalAttributeString",
				"0..1",
				[
					XmlAttribute('typeLabel', 'loudnessMethod'),
					XmlAttribute('typeNamespace', 'en17650:2022'),
					XmlAttribute('typeLink', 'urn:cen.eu:en17650:2022:ns:metadata#soundLoudness')
				],
				XmlText(Constants.unknown_metadata_place_holder)))
		self.entries.append(XmlEntry(
				None,
				"technicalAttributeFloat",
				"0..1",
				[
					XmlAttribute('typeLabel', 'integratedLoudness')
				],
				XmlText(Constants.unknown_metadata_place_holder)))
		self.entries.append(XmlEntry(
				None,
				"technicalAttributeUri",
				"1",
				[XmlAttribute('typeLabel', 'audioCodecStandardReference')],
				XmlText(Constants.unknown_metadata_place_holder)))

	def collect_metadata(self, media_source_file, xml_metadata_file_in):
		rv = RV.info('Collecting metadata for AudioFormat')

		rv_sub, cpp_metadata_elem = self.match_xpath_metadata(media_source_file, SoundFormatMatch, xml_metadata_file_in)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv, None
		rv.message = 'Successfully collected metadata for AudioFormat'
		return rv, cpp_metadata_elem
