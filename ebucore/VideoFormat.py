from ebucore.XmlTypes import XmlEntry, XmlAttribute, XmlText
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore_match.VideoFormatMatch import VideoFormatMatch
from RV import RV


class VideoFormat(AbstractFormat):
	def __init__(self):
		AbstractFormat.__init__(self, name="videoFormat", namespace='ebucore')

		self.entries.append(XmlEntry(
			None,
			"regionDelimX",
			"0..1",
			[],
			XmlText(Constants.unknown_optional_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"regionDelimY",
			"0..1",
			[],
			XmlText(Constants.unknown_optional_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"width",
			"0..1",
			[],
			XmlText(Constants.unknown_optional_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"height",
			"0..1",
			[],
			XmlText(Constants.unknown_optional_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"frameRate",
			"1",
			[
				XmlAttribute('factorNumerator', Constants.unknown_optional_metadata_place_holder),
				XmlAttribute('factorDenominator', Constants.unknown_optional_metadata_place_holder)
			],
			XmlText(Constants.unknown_metadata_place_holder)))

		self.entries.append(XmlEntry(
				[
					XmlEntry(
						None,
						"factorNumerator",
						"0..1",
						[],
						XmlText(Constants.unknown_optional_metadata_place_holder)),
					XmlEntry(
						None,
						"factorDenominator",
						"0..1",
						[],
						XmlText(Constants.unknown_optional_metadata_place_holder)),
				],
				"aspectRatio",
				"0..1",
				[],
				None))
		self.entries.append(XmlEntry(
			None,
			"technicalAttributeString",
			"1",
			[XmlAttribute('typeLabel', 'videoCodecStandardReference')],
			XmlText(Constants.unknown_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"technicalAttributeString",
			"1",
			[XmlAttribute('typeLabel', 'colourReference')],
			XmlText(Constants.unknown_metadata_place_holder)))
		self.entries.append(XmlEntry(
			None,
			"technicalAttributeUnsignedByte",
			"0..1",
			[
				XmlAttribute('typeLabel', 'progressiveMode')
			],
			XmlText(Constants.unknown_optional_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"technicalAttributeUnsignedByte",
			"0..1",
			[
				XmlAttribute('typeLabel', 'chromaSubsampling')
			],
			XmlText(Constants.unknown_optional_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"technicalAttributeUnsignedInteger",
			"1",
			[XmlAttribute('typeLabel', 'componentNumber')],
			XmlText(Constants.unknown_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"technicalAttributeUnsignedInteger",
			"1",
			[XmlAttribute('typeLabel', 'componentBitdepth')],
			XmlText(Constants.unknown_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"technicalAttributeUnsignedInteger",
			"1",
			[XmlAttribute('typeLabel', 'channelNumber')],
			XmlText(Constants.unknown_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"technicalAttributeUnsignedInteger",
			"1",
			[XmlAttribute('typeLabel', 'colourPrimaries')],
			XmlText(Constants.unknown_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"technicalAttributeUnsignedInteger",
			"1",
			[XmlAttribute('typeLabel', 'transferCharacteristics')],
			XmlText(Constants.unknown_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"technicalAttributeUnsignedInteger",
			"1",
			[XmlAttribute('typeLabel', 'matrixCoefficients')],
			XmlText(Constants.unknown_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"technicalAttributeUnsignedInteger",
			"1",
			[XmlAttribute('typeLabel', 'fullRangeFlag')],
			XmlText(Constants.unknown_metadata_place_holder)))
		self.entries.append(XmlEntry(
			None,
			"technicalAttributeUnsignedInteger",
			"0..1",
			[XmlAttribute('typeLabel', 'luminanceMin')],
			XmlText(Constants.unknown_optional_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"technicalAttributeUnsignedInteger",
			"0..1",
			[XmlAttribute('typeLabel', 'luminanceMax')],
			XmlText(Constants.unknown_optional_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"technicalAttributeUnsignedInteger",
			"0..1",
			[XmlAttribute('typeLabel', 'valueRangeMin')],
			XmlText(Constants.unknown_optional_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"technicalAttributeUnsignedInteger",
			"0..1",
			[XmlAttribute('typeLabel', 'valueRangeMax')],
			XmlText(Constants.unknown_optional_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"technicalAttributeUri",
			"1",
			[XmlAttribute('typeLabel', 'colourReference')],
			XmlText(Constants.unknown_metadata_place_holder)))

		self.entries.append(XmlEntry(
			None,
			"technicalAttributeUri",
			"1",
			[XmlAttribute('typeLabel', 'videoCodecStandardReference')],
			XmlText(Constants.unknown_metadata_place_holder)))

	def collect_metadata(self, media_source_file, xml_metadata_file_in):
		rv = RV.info('Collecting metadata for VideoFormat')
		rv_sub, cpp_metadata_elem = self.match_xpath_metadata(media_source_file, VideoFormatMatch, xml_metadata_file_in)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv, None
		return rv, cpp_metadata_elem
