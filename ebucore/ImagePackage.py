from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore.ImageFormat import ImageFormat
from ebucore.ImagePackageContainerFormat import ImagePackageContainerFormat
from ebucore.XmlTypes import XmlEntry, XmlAttribute, XmlText
from ebucore_match.ImagePackageFormatMatch import ImagePackageFormatMatch
import os
from RV import RV


class ImagePackage(AbstractFormat):

	def __init__(self):
		AbstractFormat.__init__(self, name="coreMetadata", namespace='ebucore')

		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"title",
					"0..1",
					[],
					XmlText(Constants.unknown_optional_metadata_place_holder),
					namespace='dc')],
				"title",
				"0..1",
				[XmlAttribute('typeLabel', "subpackageName")],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"description",
					"1",
					[],
					XmlText(Constants.unknown_optional_metadata_place_holder),
					namespace='dc')],
				"description",
				"0..1",
				[
					XmlAttribute('typeLabel', "fileOrigin"),
					XmlAttribute('typeNamespace', "en17650:2022"),
					XmlAttribute('typeLink', 'urn:cen.eu:en17650:2022:ns:metadata#commonFileOrigin')
				],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"description",
					"1",
					[],
					XmlText(Constants.unknown_optional_metadata_place_holder),
					namespace='dc')],
				"description",
				"0..1",
				[
					XmlAttribute('typeLabel', "userText")
				],
				None))
		# imagePackageFormat
		self.entries.append(
			XmlEntry(
				[
					XmlEntry(
								[XmlEntry(
									None,
									"editUnitNumber",
									1,
									[],
									XmlText(Constants.unknown_metadata_place_holder))],
								"duration",
								1,
								[XmlAttribute('typeLabel', 'durationInFrames')],
								None),
					XmlEntry(
								None,
								"editRate",
								"1",
								[XmlAttribute('factorNumerator', Constants.unknown_metadata_place_holder),
								 XmlAttribute('factorDenominator', Constants.unknown_metadata_place_holder)],
								XmlText(Constants.unknown_metadata_place_holder)),
					XmlEntry(None,
							 "technicalAttributeString",
							 "1",
							 [XmlAttribute('typeLabel', 'filenamePrefix')],
							 XmlText(Constants.unknown_metadata_place_holder)),
					XmlEntry(
								None, "technicalAttributeString",
								"1",
								[XmlAttribute('typeLabel', 'filenameExtension')],
								XmlText(Constants.unknown_metadata_place_holder)),
					XmlEntry(None,
								 "technicalAttributeUnsignedByte",
								 "1",
								 [XmlAttribute('typeLabel', 'filenameDigits')],
								 XmlText(Constants.unknown_metadata_place_holder)),
					XmlEntry(
								 None,
								 "technicalAttributeUnsignedInteger",
								 "1",
								 [XmlAttribute('typeLabel', 'firstIndex')],
								 XmlText(Constants.unknown_metadata_place_holder)),
					XmlEntry(
								None, "technicalAttributeUnsignedInteger",
								"1",
								[XmlAttribute('typeLabel', 'lastIndex')],
								XmlText(Constants.unknown_metadata_place_holder)),
					XmlEntry(
								None,
								"technicalAttributeUnsignedInteger",
								"0..1",
								[XmlAttribute('typeLabel', 'firstActiveIndex')],
								XmlText(Constants.unknown_optional_metadata_place_holder)),
					XmlEntry(
								None,
								"technicalAttributeUnsignedInteger",
								"0..1",
								[XmlAttribute('typeLabel', 'lastActiveIndex')],
								XmlText(Constants.unknown_optional_metadata_place_holder))
				],
				"format",
				"1",
				[XmlAttribute('formatName', 'imagePackageFormat')],
				None))
		# containerFormat
		# ToDo: Check if this element is just a placeholder (I guess it is). If yes, reduce to one entry and add proper description here
		self.entries.append(
			XmlEntry(
				[
					XmlEntry(
						None,
						"mimeType",
						"0..1",
						[XmlAttribute('typeLabel', Constants.unknown_optional_metadata_place_holder)],
						None)
				],
				"format",
				"1",
				[XmlAttribute('formatName', 'containerFormat')],
				None))
		# imageFormat
		# ToDo: Check if this element is just a placeholder (I guess it is). If yes, reduce to one entry and add proper description here
		self.entries.append(
			XmlEntry(
				[],
				"format",
				"1",
				[XmlAttribute('formatName', 'imageFormat')],
				None))

	def collect_metadata(self, media_source_file, xml_metadata_file_in):
		rv = RV.info('Collecting metadata for ' + self.name)
		if os.path.isdir(media_source_file):
			for f in sorted(os.listdir(media_source_file)):
				if f in [".DS_Store"]:
					continue
				media_source_file = os.path.join(media_source_file, f)
				break

		rv_sub, image_package_elem = self.match_xpath_metadata(
				media_source_file, ImagePackageFormatMatch, xml_metadata_file_in)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv, None

		cf_elem = image_package_elem.find(
			"./ebucore:format[@formatName='containerFormat']", Constants.ns)
		if cf_elem is None:
			rv.add_child(RV.error('Container Format element is None'))
			return rv, None
		else:
			# generate and append containerFormat
			container_format = ImagePackageContainerFormat()
			rv_sub, elem = container_format.collect_metadata(media_source_file, xml_metadata_file_in)

			# rv_sub, elem = container_format.collect_metadata(media_source_file, xml_metadata_file_in)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, None
			cf_elem.insert(0, elem)

		if_elem = image_package_elem.find(
			"./ebucore:format[@formatName='imageFormat']", Constants.ns)
		if if_elem is None:
			rv.add_child(RV.error('Image Format element is None'))
			return rv, None
		else:
			# generate and append containerFormat
			image_format = ImageFormat()
			rv_sub, elem = image_format.collect_metadata(media_source_file, xml_metadata_file_in)

			# rv_sub, elem = image_format.collect_metadata(media_source_file, xml_metadata_file_in)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, None
			if_elem.insert(len(if_elem), elem)

		# ------------------------------------------------------------
		# set dc title with subpackage name
		# ------------------------------------------------------------
		sub_package_data_folder_abs = os.path.abspath(os.path.join(media_source_file, os.pardir))

		if sub_package_data_folder_abs.endswith('/data'):
			sub_package_folder_abs = os.path.abspath(os.path.join(sub_package_data_folder_abs, os.pardir))
			dc_title = os.path.basename(sub_package_folder_abs)
			dc_title_elem = image_package_elem.find("./ebucore:title/dc:title", Constants.ns)
			dc_title_elem.text = dc_title
		else:
			rv.add_child(RV.warning('File not in subpackage\'s data sub-folder, path is: ' + str(media_source_file)))

		# rv.message = 'Successfully collected metadata for ' + self.name
		return rv, image_package_elem

	'''
	def validate(self, source_element, abs_xpath, line_offset):
		rv = RV.info('Validating ImagePackage')

		local_xpath = './ebucore:format[@formatName="imagePackageFormat"]/ebucore:imagePackageFormat'

		image_package_format_elem = source_element.xpath(local_xpath, namespaces=Constants.ns)
		if len(image_package_format_elem) == 0:
			return rv.add_child(
				RV.error('no imagePackageFormat format element found in: ' + local_xpath))

		ipf = ImagePackageFormat()
		line_offset = 0

		rv_sub = ipf.validate(image_package_format_elem[0], abs_xpath, line_offset)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)

		local_xpath = './ebucore:format[@formatName="containerFormat"]/ebucore:containerFormat'
		container_format_elem = source_element.xpath(local_xpath, namespaces=Constants.ns)
		if len(container_format_elem) == 0:
			return rv.add_child(
				RV.error('no containerFormat format element found in: ' + local_xpath))

		ipcf = ImagePackageContainerFormat()
		line_offset = 0

		rv_sub = ipcf.validate(container_format_elem[0], abs_xpath, line_offset)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)

		local_xpath = './ebucore:format[@formatName="imageFormat"]/ebucore:imageFormat'
		image_format_elem = source_element.xpath(local_xpath, namespaces=Constants.ns)
		if len(image_package_format_elem) == 0:
			return rv.add_child(
				RV.error('no imageFormat format element found in: ' + local_xpath))

		img_format = ImageFormat()
		line_offset = 0

		rv_sub = img_format.validate(image_format_elem[0], abs_xpath, line_offset)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)

		return rv
	'''
