from AbstractFormat import AbstractFormat
from ebucore.AudioFormat import AudioFormat
from ebucore.Constants import Constants
from ebucore.SoundPackageContainerFormat import SoundPackageContainerFormat
from ebucore.SoundPackageFormat import SoundPackageFormat
from ebucore.XmlTypes import XmlEntry, XmlAttribute, XmlText
from RV import RV
import os


class SoundPackage(AbstractFormat):
	def __init__(self):
		AbstractFormat.__init__(self, name="coreMetadata", namespace='ebucore')
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"title",
					"0..1",
					[],
					XmlText(Constants.unknown_optional_metadata_place_holder),
					namespace='dc')],
				"title",
				"0..1",
				[XmlAttribute('typeLabel', "subpackageName")],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"description",
					"1",
					[],
					XmlText(Constants.unknown_optional_metadata_place_holder),
					namespace='dc')],
				"description",
				"0..1",
				[
					XmlAttribute('typeLabel', "fileOrigin"),
					XmlAttribute('typeNamespace', "en17650:2022"),
					XmlAttribute('typeLink', 'urn:cen.eu:en17650:2022:ns:metadata#commonFileOrigin')
				],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"description",
					"1",
					[],
					XmlText(Constants.unknown_optional_metadata_place_holder),
					namespace='dc')],
				"description",
				"0..1",
				[
					XmlAttribute('typeLabel', "userText")
				],
				None))

		self.entries.append(
			XmlEntry(
				None,
				"format",
				"1",
				[XmlAttribute('formatName', 'soundPackageFormat')],
				None))
		self.entries.append(
			XmlEntry(
				[
					XmlEntry(
						None,
						"mimeType",
						"0..1",
						[XmlAttribute('typeLabel', Constants.unknown_optional_metadata_place_holder)],
						None)
				],
				"format",
				"1",
				[XmlAttribute('formatName', 'containerFormat')],
				None))
		self.entries.append(
			XmlEntry(
				None,
				"format",
				"1",
				[XmlAttribute('formatName', 'audioFormat')],
				None))

	def collect_metadata(self, media_source_file, xml_metadata_file_in):
		rv = RV.info('Collecting metadata for ' + self.name)

		rv_sub, sound_package_elem = self.gen_stub()
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv, None

		# append soundPackageFormatTag
		spf_elem = sound_package_elem.find("./ebucore:format[@formatName='soundPackageFormat']", Constants.ns)
		if spf_elem is None:
			rv.add_child(RV.error('Sound Package Format element is None - unexpected behaviour. Aborting'))
			return rv, None
		else:
			# generate and append imagePackageFormat
			sound_package_format = SoundPackageFormat()
			rv_sub, elem = sound_package_format.collect_metadata(media_source_file, xml_metadata_file_in)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, None
			insert_cnt = 0
			for child in elem:
				spf_elem.insert(insert_cnt, child)
				insert_cnt += 1

			from lxml import etree
			text_rep = etree.tostring(spf_elem, pretty_print=True, encoding='utf8', method='xml').decode()
			# print(text_rep)

		cf_elem = sound_package_elem.find(
			"./ebucore:format[@formatName='containerFormat']", Constants.ns)
		if cf_elem is None:
			rv.add_child(RV.error('Container Format element is None'))
			return rv, None
		else:
			# generate and append containerFormat
			container_format = SoundPackageContainerFormat()
			rv_sub, elem = container_format.collect_metadata(media_source_file, xml_metadata_file_in)

			# rv_sub, elem = container_format.collect_metadata(media_source_file, xml_metadata_file_in)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, None
			cf_elem.insert(0, elem)
		'''
		cf_elem = sound_package_elem.find("./ebucore:format[@formatName='containerFormat']", Constants.ns)
		if cf_elem is None:
			rv.add_child(RV.error('Container Format element is None - unexpected behaviour. Aborting'))
			return rv, None
		else:
			# generate and append containerFormat
			container_format = SoundPackageContainerFormat()
			rv_sub, elem = container_format.collect_metadata(media_source_file, xml_metadata_file_in)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, None
			insert_cnt = 0
			for child in elem:
				cf_elem.insert(insert_cnt, child)
				insert_cnt += 1
		'''
		sf_elem = sound_package_elem.find("./ebucore:format[@formatName='audioFormat']", Constants.ns)
		if sf_elem is None:
			rv.add_child(RV.error('Audio Format element is None - unexpected behaviour. Aborting'))
			return rv, None
		else:
			# generate and append containerFormat
			# 10.3.1.4.5
			audio_format = AudioFormat()

			rv_sub, elem = audio_format.collect_metadata(media_source_file, xml_metadata_file_in)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, None
			sf_elem.insert(0, elem)

			audio_format_elem = \
				sound_package_elem.find("./ebucore:format[@formatName='audioFormat']/ebucore:audioFormat", Constants.ns)

			audio_format_elem.set('audioFormatName', Constants.unknown_metadata_place_holder)
			audio_format_elem.set('audioFormatVersionId', Constants.unknown_metadata_place_holder)

		# ------------------------------------------------------------
		# set dc title with subpackage name
		# ------------------------------------------------------------
		sub_package_data_folder_abs = os.path.abspath(os.path.join(media_source_file, os.pardir))

		if sub_package_data_folder_abs.endswith('/data'):
			sub_package_folder_abs = os.path.abspath(os.path.join(sub_package_data_folder_abs, os.pardir))
			dc_title = os.path.basename(sub_package_folder_abs)
			dc_title_elem = sound_package_elem.find("./ebucore:title/dc:title", Constants.ns)
			dc_title_elem.text = dc_title
		else:
			rv.add_child(RV.warning('File not in subpackage\'s data sub-folder, path is: ' + str(media_source_file)))

		return rv, sound_package_elem
