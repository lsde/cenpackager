from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore_match.TimedTextPackageFormatMatch import TimedTextPackageFormatMatch
from ebucore.XmlTypes import XmlEntry, XmlAttribute, XmlText
from RV import RV
import os


class TimedTextPackage(AbstractFormat):
	def __init__(self):
		AbstractFormat.__init__(self, name="coreMetadata", namespace='ebucore')
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"title",
					"0..1",
					[],
					XmlText(Constants.unknown_optional_metadata_place_holder),
					namespace='dc')],
				"title",
				"0..1",
				[XmlAttribute('typeLabel', "subpackageName")],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"description",
					"1",
					[],
					XmlText(Constants.unknown_optional_metadata_place_holder),
					namespace='dc')],
				"description",
				"0..1",
				[
					XmlAttribute('typeLabel', "fileOrigin"),
					XmlAttribute('typeNamespace', "en17650:2022"),
					XmlAttribute('typeLink', 'urn:cen.eu:en17650:2022:ns:metadata#commonFileOrigin')
				],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"description",
					"1",
					[],
					XmlText(Constants.unknown_optional_metadata_place_holder),
					namespace='dc')],
				"description",
				"0..1",
				[
					XmlAttribute('typeLabel', "userText")
				],
				None))
		self.entries.append(
			XmlEntry(
				[
					XmlEntry(
						None,
						"language",
						"1..*",
						[],
						XmlText(Constants.unknown_metadata_place_holder),
						namespace='dc')
				],
				"language",
				"1",
				[
					XmlAttribute('typeNamespace', Constants.unknown_metadata_place_holder),
					XmlAttribute('typeLink', Constants.unknown_metadata_place_holder)
				],
				None))
		self.entries.append(
			XmlEntry(
				[
					XmlEntry(
							None,
							"mimeType",
							"0..1",
							[XmlAttribute('typeLabel', Constants.unknown_optional_metadata_place_holder)],
							None),
					XmlEntry(
							None,
							"technicalAttributeString",
							"0..1",
							[XmlAttribute('typeLabel', 'timedTextStandardReference')],
							XmlText(Constants.unknown_optional_metadata_place_holder)),

					XmlEntry(
							None,
							"technicalAttributeString",
							"1",
							[XmlAttribute('typeLabel', 'subtitleDestination')],
							XmlText(Constants.unknown_metadata_place_holder)),
					XmlEntry(
							None,
							"technicalAttributeString",
							"0..1",
							[XmlAttribute('typeLabel', 'characterEncoding')],
							XmlText(Constants.unknown_optional_metadata_place_holder)),
					XmlEntry(
							None,
							"technicalAttributeBoolean",
							"0..1",
							[XmlAttribute('typeLabel', 'useSubpictures')],
							XmlText(Constants.unknown_optional_metadata_place_holder)),
					XmlEntry(
							None,
							"technicalAttributeUri",
							"0..1",
							[XmlAttribute('typeLabel', 'timedTextStandardReference')],
							XmlText(Constants.unknown_optional_metadata_place_holder))
				],
				"format",
				"1",
					[XmlAttribute('formatName', 'timedTextPackageFormat')],
				None))

	def collect_metadata(self, media_source_file, xml_metadata_file_in):
		rv = RV.info('Collecting metadata for ' + self.name)

		rv_sub, tt_package_elem = \
			self.match_xpath_metadata(media_source_file, TimedTextPackageFormatMatch, xml_metadata_file_in)

		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv, None

		# ------------------------------------------------------------
		# set dc title with subpackage name
		# ------------------------------------------------------------
		sub_package_data_folder_abs = os.path.abspath(os.path.join(media_source_file, os.pardir))

		if sub_package_data_folder_abs.endswith('/data'):
			sub_package_folder_abs = os.path.abspath(os.path.join(sub_package_data_folder_abs, os.pardir))
			dc_title = os.path.basename(sub_package_folder_abs)
			dc_title_elem = tt_package_elem.find("./ebucore:title/dc:title", Constants.ns)
			dc_title_elem.text = dc_title
		else:
			rv.add_child(RV.warning('File not in subpackage\'s data sub-folder, path is: ' + str(media_source_file)))

		return rv, tt_package_elem
