class XmlText:
    def __init__(self, default_value):
        self.default_value = default_value


class XmlAttribute:
    def __init__(self, name, default_value, namespace=''):
        self.name = name
        self.default_value = default_value
        self.namespace = namespace


class XmlEntry:
    def __init__(self, child_elements, name, arity, attributes, text_value, namespace='ebucore'):
        self.child_elements = child_elements
        self.name = name
        self.arity = arity
        self.attributes = attributes
        self.text_value = text_value
        self.namespace = namespace
