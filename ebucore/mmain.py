import sys
import os
from ebucore.ImageFormat import ImageFormat
from ebucore.SoundPackageFormat import SoundPackageFormat
from ebucore.CoreMetadataWorkDescriptive import CoreMetadataDescriptive
from ebucore.CoreMetadataVariantDescriptive import CoreMetadataVariantDescriptive
from ebucore.AudioFormat import AudioFormat
from ebucore.TimedTextPackageFormat__OBSOLETE__ import TimedTextPackageFormat
from ebucore.AudiovisualPackageFormat__OBSOLETE__ import AudiovisualPackageFormat
from ebucore.VideoFormat import VideoFormat
from ebucore.ComponentizedPackageFormat_OBSOLETE_ import ComponentizedPackageFormat
from ebucore.CplFormat__OBSOLETE__ import CplFormat
from ebucore.ImagePackage import ImagePackage
from ebucore.SoundPackage import SoundPackage
from ebucore.TimedTextPackage import TimedTextPackage
from ebucore.AudiovisualPackage import AudiovisualPackage
from ebucore.ComponentizedPackage import ComponentizedPackage


from lxml import etree
from utils.log import log


def main(argv):

	source_file = '/Users/sbg/Documents/python_playground/cenpackager/unittest/test_material/short_j2c_clip/image_000000.j2c'
	# source_file = '/Users/sbg/Movies/Stage.mp4'
	# source_file = '/Users/sbg/Music/Multichannel.wav'
	source_file = '/Users/sbg/Desktop/Example Subtitles/Example Subtitle SMPTE with LoadFont.xml'
	source_file = '/Users/sbg/Desktop/Example Subtitles/Netflix_Plugfest_Oct2015_FULL_Vers_IMSC.xml'
	source_file = \
		'/Users/sbg/Movies/Asdf_FTR_F_EN-XX_INT_51_2K_NULL_20210217_NUL_SMPTE_OV/' \
		'CPL_c0a64fa8-d38c-4103-a939-df549038c4fb.xml'

	source_file = \
		'/Users/sbg/Movies/Asdf_FTR_F_EN-XX_INT_51_2K_NULL_20210217_NUL_IOP_OV/' \
		'CPL_fc06f6e8-2470-4dd7-9b9f-478a9a26fd5c.xml'

	source_file = \
		'/Users/sbg/Movies/App2_FTR_F_EN-XX_INT_51_4K_NULL_20210217_NUL_SMPTE_OV/' \
		'CPL_1efeefb2-e2ce-4f2d-a60f-827e3e737d02.xml'

	source_file = \
		'/Users/sbg/Movies/App2E_FTR_F_EN-XX_INT_51_4K_NULL_20210217_NUL_SMPTE_OV/' \
		'CPL_d47a10d3-3b21-45dd-a8e0-9c936cf7c5db.xml'

	if os.path.exists(source_file) is False:
		log.error('Given media source file does not exists: ' + source_file)
		return

	cmd = ComponentizedPackage()
	d = cmd.collect_metadata(media_source_file=source_file, xml_metadata_file_in=None)
	if d is None:
		return
	text_rep = etree.tostring(d, pretty_print=True, encoding='utf8', method='xml').decode()
	print(text_rep)
	return

	imf = TimedTextPackage()
	cen_metadata_xml = imf.collect_metadata(media_source_file=source_file, xml_metadata_file_in=None)

	xml_metadata_file_out = None
	# save xml file
	if xml_metadata_file_out is not None:
		et = etree.ElementTree(cen_metadata_xml)
		et.write(xml_metadata_file_out, pretty_print=True, xml_declaration=True, encoding="utf-8")
	else:
		log.warning('No path for XML file output provided. Printing to console instead')
		text_rep = etree.tostring(cen_metadata_xml, pretty_print=True, encoding='utf8', method='xml').decode()
		print(text_rep)


if __name__ == "__main__":
	main(sys.argv)
