

class ColorSpace:
	def __init__(
			self, definition, colour_primaries, transfer_characteristics, matrix_coefficients, video_full_rage_flag):
		self.definition = definition
		self.colour_primaries = colour_primaries
		self.transfer_characteristics = transfer_characteristics
		self.matrix_coefficients = matrix_coefficients
		self.video_full_rage_flag = video_full_rage_flag


class ColorSpaceDefinition:
	spaces = list()
	spaces.append(
		ColorSpace(
			definition='IEC 61966-2-1 sRGB',
			colour_primaries='1',
			transfer_characteristics='13',
			matrix_coefficients='0',
			video_full_rage_flag='0')
		)
	spaces.append(
		ColorSpace(
			definition='IEC 61966-2-1 sYCC',
			colour_primaries='1',
			transfer_characteristics='13',
			matrix_coefficients='1',
			video_full_rage_flag='0')
	)
	spaces.append(
		ColorSpace(
			definition='Rec.ITU-R BT.709-6',
			colour_primaries='1',
			transfer_characteristics='1',
			matrix_coefficients='1',
			video_full_rage_flag='0 or 1')
	)
	spaces.append(
		ColorSpace(
			definition='Rec.ITU-R BT.601-7 625',
			colour_primaries='5',
			transfer_characteristics='6',
			matrix_coefficients='5',
			video_full_rage_flag='0 or 1')
	)
	spaces.append(
		ColorSpace(
			definition='Rec.ITU-R BT.601-7 525',
			colour_primaries='6',
			transfer_characteristics='6',
			matrix_coefficients='6',
			video_full_rage_flag='0 or 1')
	)
	spaces.append(
		ColorSpace(
			definition='Rec.ITU-R BT.2020-2',
			colour_primaries='9',
			transfer_characteristics='14 or 15',
			matrix_coefficients='9 or 10',
			video_full_rage_flag='0 or 1')
	)
	spaces.append(
		ColorSpace(
			definition='Rec.ITU-R BT.2100-0',
			colour_primaries='9',
			transfer_characteristics='16 or 18',
			matrix_coefficients='9',
			video_full_rage_flag='0 or 1')
	)
	spaces.append(
		ColorSpace(
			definition='SMPTE ST 428-1 CIE 1931 XYZ ISO 11664-1',
			colour_primaries='10',
			transfer_characteristics='17',
			matrix_coefficients='0',
			video_full_rage_flag='0')
	)
	spaces.append(
		ColorSpace(
			definition='SMPTE RP 431-2 (2011)',
			colour_primaries='11',
			transfer_characteristics='17',
			matrix_coefficients='0',
			video_full_rage_flag='0')
	)
	spaces.append(
		ColorSpace(
			definition='SMPTE RP 431-2 (2010)',
			colour_primaries='12',
			transfer_characteristics='17',
			matrix_coefficients='0',
			video_full_rage_flag='0')
	)