from Content.AbstractPackage import AbstractPackage
from RV import RV
import os
from lxml import etree
from ebucore.ImagePackage import ImagePackage as EbuCoreImagePackage
from ebucore.EbuCoreMain import EbuCoreMain


class ExtraPackage(AbstractPackage):
	def __init__(self):
		AbstractPackage.__init__(self)

	@staticmethod
	def write_subpackage_metadata(sub_package_uuid, path_subpackage):
		rv = RV.info('Writing subpackage metadata for subpackage: ' + path_subpackage)

		# According to 8.6.5 Technical Metadata
		# No technical metadata file shall be present.
		return rv
