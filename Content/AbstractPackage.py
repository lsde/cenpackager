import errno
import shutil
from RV import RV


class AbstractPackage:

	def __init__(self):
		pass

	'''
	@staticmethod
	def generate_file_grp_entries(sub_package_id, sub_package_data_folder, file_grp_xml_elem):
		rv = RV.info('Generating file grp entries')
		file_prt_list = []
		for data_file in sorted(os.listdir(sub_package_data_folder)):

			if data_file in ['.DS_Store']:
				continue

			full_data_file_path = os.path.join(sub_package_data_folder,  data_file)

			file_instance = File()
			rv_sub, file_elem = file_instance.gen_stub()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, None

			# ---------------------------------------------------------------------------
			# get FLocat element
			# ---------------------------------------------------------------------------
			file_locat_elem = file_elem.find('./mets:FLocat', Constants.ns)
			if file_locat_elem is None:
				rv.add_child(RV.error('Cannot find FLocat Element in generated METS stub.'))
				return rv, None

			file_locat_elem.set('{%s}href' % Constants.ns["xlink"], os.path.join('data', data_file))

			numbering_info = get_file_numbering_info(full_data_file_path)
			number = str(numbering_info[1]).zfill(numbering_info[0])
			file_ptr_id = 'data_' + sub_package_id + '_' + number
			file_elem.set('ID', file_ptr_id)
			file_prt_list.append(file_ptr_id)
			file_elem.set('MIMETYPE', magic.from_file(full_data_file_path, mime=True))
			file_elem.set('SIZE', str(os.path.getsize(full_data_file_path)))
			file_elem.set('CHECKSUM', Hash.get_string_representation(full_data_file_path))
			file_elem.set('CHECKSUMTYPE', 'SHA-256')

			file_grp_xml_elem.insert(len(file_grp_xml_elem), file_elem)

		rv.message = 'Successfully generated file grp entries'
		return rv, file_prt_list

	'''

	@staticmethod
	def copy_subpackage_data(path_source_data, path_subpackage_data_folder):
		rv = RV.info('Copying data to subpackage folder: ' + path_subpackage_data_folder)
		# copy source data to new generated folder
		try:
			shutil.copytree(path_source_data, path_subpackage_data_folder)
		except OSError as e:
			# If the error was caused because the source wasn't a directory
			if e.errno == errno.ENOTDIR:
				shutil.copy(path_source_data, path_subpackage_data_folder)
			else:
				return rv.add_child(RV.error('Directory not copied. Error: %s' % e))

		rv.message = 'Successfully copied data to subpackage folder: ' + path_subpackage_data_folder
		return rv

	@staticmethod
	def write_subpackage_metadata(sub_package_uuid, path_subpackage):
		rv = RV.info('Writing subpackage metadata for subpackage: ' + path_subpackage)
		return rv.add_child(RV.warning(
			'Calling AbstractPackage function. No implementation for writing subpackage metadata'))
