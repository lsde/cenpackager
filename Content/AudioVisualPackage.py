from Content.AbstractPackage import AbstractPackage
from RV import RV
import os
from lxml import etree
from ebucore.AudiovisualPackage import AudiovisualPackage as AudiovisualPackageMD
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore.EbuCoreMain import EbuCoreMain


class AudioVisualPackage(AbstractPackage):
    def __init__(self):
        AbstractPackage.__init__(self)

    @staticmethod
    def has_audio(path_av_file):
        rv, media_info_xml = AbstractFormat.xml(path_av_file)
        d = media_info_xml.xpath(
            '//ebucore:ebuCoreMain/ebucore:coreMetadata/ebucore:format/ebucore:audioFormat', namespaces=Constants.ns)
        if len(d) > 0:
            return True
        else:
            return False

    @staticmethod
    def write_subpackage_metadata(sub_package_uuid, path_subpackage):
        rv = RV.info('Writing subpackage metadata for subpackage: ' + path_subpackage)

        path_subpackage_data_folder = os.path.join(path_subpackage, 'data')
        path_subpackage_metadata_folder = os.path.join(path_subpackage, 'metadata')

        ebucore_main = EbuCoreMain()
        rv_sub, ebucore_main_md = ebucore_main.collect_metadata(
            media_source_file=None,
            xml_metadata_file_in=None)
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv

        md_file_counter = 0

        for data_file in os.listdir(path_subpackage_data_folder):
            if data_file.startswith('.'):
                continue

            if md_file_counter > 1:
                return rv.add_child(RV.error(
                    'More than one file in data folder while adding audio visual package: ' +
                    str(path_subpackage_data_folder)))
            full_data_path = os.path.join(path_subpackage_data_folder, data_file)
            audio_visual_package_md = AudiovisualPackageMD()
            rv_sub, audio_visual_md_elem = audio_visual_package_md.collect_metadata(full_data_path, None)
            if not rv_sub.is_info():
                rv.add_child(rv_sub)
                if rv_sub.is_error():
                    return rv
            if audio_visual_md_elem is not None:
                file_base_name = 'techMD_' + str(sub_package_uuid) + '_' + ('%04d' % md_file_counter) + '.xml'
                md_file_counter += 1
                metadata_file_name = os.path.join(path_subpackage_metadata_folder, file_base_name)

                ebucore_main_md.insert(len(ebucore_main_md), audio_visual_md_elem)

                tree = etree.ElementTree(ebucore_main_md)
                tree.write(metadata_file_name, pretty_print=True, xml_declaration=True, encoding="utf-8")
            else:
                return rv.add_child(RV.error('While trying to collect metadata. Returned XML element is None'))

        # rv.message = 'Successfully written subpackage metadata for subpackage: ' + path_subpackage
        return rv
