from Content.AbstractPackage import AbstractPackage
from RV import RV
from lxml import etree
import os
from ebucore.TimedTextPackage import TimedTextPackage as TimedTextPackageMD
from ebucore.EbuCoreMain import EbuCoreMain
import errno
import shutil


class TimedTextPackage(AbstractPackage):
    def __init__(self):
        AbstractPackage.__init__(self)

    @staticmethod
    def copy_subpackage_data(path_source_data, path_subpackage_data_folder):
        rv = RV.info('Copying data to subpackage folder: ' + path_subpackage_data_folder)
        # copy source data to new generated folder
        try:
            shutil.copytree(path_source_data, path_subpackage_data_folder)
        except OSError as e:
            # If the error was caused because the source wasn't a directory
            if e.errno == errno.ENOTDIR:
                shutil.copy(path_source_data, path_subpackage_data_folder)
            else:
                return rv.add_child(RV.error('Directory not copied. Error: %s' % e))

        rv.message = 'Successfully copied data to subpackage folder: ' + path_subpackage_data_folder
        return rv

    @staticmethod
    def write_subpackage_metadata(sub_package_uuid, path_subpackage):
        rv = RV.info('Writing subpackage metadata for subpackage: ' + path_subpackage)

        path_subpackage_data_folder = os.path.join(path_subpackage, 'data')
        path_subpackage_metadata_folder = os.path.join(path_subpackage, 'metadata')

        ebucore_main = EbuCoreMain()
        rv_sub, ebucore_main_md = ebucore_main.collect_metadata(
            media_source_file=None,
            xml_metadata_file_in=None)
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv

        md_file_counter = 0

        for data_file in os.listdir(path_subpackage_data_folder):
            if data_file.startswith('.'):
                continue

            if not data_file.endswith('.xml'):
                continue

            timed_text_package = TimedTextPackageMD()
            rv_sub, timed_text_package_md_elem = \
                timed_text_package.collect_metadata(os.path.join(path_subpackage_data_folder, data_file), None)
            if not rv_sub.is_info():
                rv.add_child(rv_sub)
                if rv_sub.is_error():
                    return rv

            if timed_text_package_md_elem is not None:
                file_base_name = 'techMD_' + str(sub_package_uuid) + '_' + ('%04d' % md_file_counter) + '.xml'
                md_file_counter += 1
                metadata_file_name = os.path.join(path_subpackage_metadata_folder, file_base_name)

                ebucore_main_md.insert(len(ebucore_main_md), timed_text_package_md_elem)

                tree = etree.ElementTree(ebucore_main_md)
                tree.write(metadata_file_name, pretty_print=True, xml_declaration=True, encoding="utf-8")
            else:
                return rv.add_child(RV.error('While trying to collect metadata. Returned XML element is None'))

        rv.message = 'Successfully written subpackage metadata for subpackage: ' + path_subpackage
        return rv
