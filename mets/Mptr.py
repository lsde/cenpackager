from ebucore.XmlTypes import XmlAttribute
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore_match.Dummy import Dummy


class Mptr(AbstractFormat):
    def __init__(self):
        AbstractFormat.__init__(self, name="mptr", namespace='mets', attributes=[
            XmlAttribute('LOCTYPE', 'URL'),
            XmlAttribute('type', 'simple', 'xlink'),
            XmlAttribute('href', Constants.unknown_metadata_place_holder, 'xlink')
        ])

    def collect_metadata(self, media_source_file, xml_metadata_file_in):
        cpp_metadata_elem = self.match_xpath_metadata(media_source_file, Dummy, xml_metadata_file_in)
        return cpp_metadata_elem
