from ebucore.XmlTypes import XmlEntry, XmlAttribute
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants


class DigiprovMd(AbstractFormat):
    def __init__(self):
        AbstractFormat.__init__(self, name="digiprovMD", namespace='mets', attributes=[
            XmlAttribute('ID', Constants.unknown_metadata_place_holder)
        ])

        self.entries.append(
            XmlEntry(
                [],
                "mdRef",
                "1",
                [
                    XmlAttribute('LOCTYPE', 'URL'),
                    XmlAttribute('href', Constants.unknown_metadata_place_holder, 'xlink'),
                    XmlAttribute('MDTYPE', 'PREMIS'),
                    XmlAttribute('MIMETYPE', 'text/xml'),
                    XmlAttribute('SIZE', Constants.unknown_metadata_place_holder),
                    XmlAttribute('CHECKSUM', Constants.unknown_metadata_place_holder),
                    XmlAttribute('CHECKSUMTYPE', Constants.unknown_metadata_place_holder)
                ],
                None,
                namespace='mets'))

    def collect_metadata(self, media_source_file, xml_metadata_file_in):
        root_elem = self.gen_stub()

        '''
        # append amdSec
        mdRef_elem = root_elem.find("./mets:amdSec", Constants.ns)
        if mdRef_elem is None:
            log.error('Timed Text Package Format element is None - unexpected behaviour. Aborting')
            return
        else:
            # generate and append imagePackageFormat
            digiprovMD = DigiprovMD()
            elem = digiprovMD.collect_metadata(media_source_file, xml_metadata_file_in)
            insert_cnt = 0
            for child in elem:
                mdRef_elem.insert(insert_cnt, child)
                insert_cnt += 1
        '''
        return root_elem
