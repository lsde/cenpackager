from ebucore.XmlTypes import XmlEntry, XmlAttribute
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore_match.Dummy import Dummy


class FileSecSubPackage(AbstractFormat):
    def __init__(self):
        AbstractFormat.__init__(self, name="fileSec", namespace='mets')

        self.entries.append(
            XmlEntry(
                [],
                "fileGrp",
                "0..1",
                [
                    XmlAttribute('ID', 'ancillaryData_' + Constants.dummy_id)
                ],
                None,
                namespace='mets'))
        self.entries.append(
            XmlEntry(
                [],
                "fileGrp",
                "1..n",
                [
                    XmlAttribute('ID', 'data_' + Constants.dummy_id)
                ],
                None,
                namespace='mets'))

    def collect_metadata(self, media_source_file, xml_metadata_file_in):
        cpp_metadata_elem = self.match_xpath_metadata(media_source_file, Dummy, xml_metadata_file_in)
        return cpp_metadata_elem
