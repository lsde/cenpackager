from ebucore.XmlTypes import XmlAttribute
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore_match.Dummy import Dummy


class MdRef(AbstractFormat):
    def __init__(self):
        AbstractFormat.__init__(self, name="mdRef", namespace='mets', attributes=[
            XmlAttribute('LOCTYPE', 'URL'),
            XmlAttribute('href', Constants.unknown_metadata_place_holder, 'xlink'),
            XmlAttribute('MDTYPE', 'OTHER'),
            XmlAttribute('OTHERMDTYPE', 'EBUCORE'),
            XmlAttribute('MIMETYPE', 'text/xml'),
            XmlAttribute('SIZE', Constants.unknown_metadata_place_holder),
            XmlAttribute('CHECKSUM', Constants.unknown_metadata_place_holder),
            XmlAttribute('CHECKSUMTYPE', Constants.unknown_metadata_place_holder)
        ])

    def collect_metadata(self, media_source_file, xml_metadata_file_in):
        cpp_metadata_elem = self.match_xpath_metadata(media_source_file, Dummy, xml_metadata_file_in)
        return cpp_metadata_elem
