from ebucore.XmlTypes import XmlEntry, XmlAttribute
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore_match.Dummy import Dummy


class StructMap(AbstractFormat):
    def __init__(self):
        AbstractFormat.__init__(self, name="structMap", namespace='mets')

        self.entries.append(
            XmlEntry(
                [
                    XmlEntry(
                        None,
                        "div",
                        "1",
                        [
                            XmlAttribute('TYPE', 'ancillaryData')
                        ],
                        None,
                        namespace='mets'
                    ),
                    XmlEntry(
                        None,
                        "div",
                        "1",
                        [
                            XmlAttribute('TYPE', 'playlists')
                        ],
                        None,
                        namespace='mets'
                    ),
                    XmlEntry(
                        None,
                        "div",
                        "1",
                        [
                            XmlAttribute('TYPE', 'subPackages')
                        ],
                        None,
                        namespace='mets'
                    )
                ],
                "div",
                "1",
                [
                    XmlAttribute('ID', 'preservationPackage_' + Constants.dummy_id),
                    XmlAttribute('TYPE', 'CPP-Package'),
                    XmlAttribute('DMDID', Constants.unknown_metadata_place_holder),
                    XmlAttribute('LABEL', Constants.unknown_metadata_place_holder),
                    XmlAttribute('ADMID', Constants.unknown_optional_metadata_place_holder)
                ],
                None,
                namespace='mets'))

    def collect_metadata(self, media_source_file, xml_metadata_file_in):
        cpp_metadata_elem = self.match_xpath_metadata(media_source_file, Dummy, xml_metadata_file_in)
        return cpp_metadata_elem
