from ebucore.XmlTypes import XmlEntry, XmlAttribute, XmlText
from AbstractFormat import AbstractFormat
from ebucore_match.Dummy import Dummy
import Config


class Hdr(AbstractFormat):
    def __init__(self):
        AbstractFormat.__init__(self, name="metsHdr", namespace='mets')

        self.entries.append(
            XmlEntry(
                [
                    XmlEntry(
                        None,
                        "name",
                        "1",
                        [],
                        XmlText(Config.APP_NAME + ' - ' + Config.APP_VERSION),
                        namespace='mets')
                ],
                "agent",
                "1",
                [
                    XmlAttribute('ROLE', 'CREATOR'),
                    XmlAttribute('TYPE', 'OTHER')
                ],
                None,
                namespace='mets'))

    def collect_metadata(self, media_source_file, xml_metadata_file_in):
        cpp_metadata_elem = self.match_xpath_metadata(media_source_file, Dummy, xml_metadata_file_in)
        return cpp_metadata_elem
